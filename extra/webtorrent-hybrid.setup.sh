#!/bin/sh

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash
sudo apt-get install -y nodejs gcc g++ make xvfb libxtst-dev libxss-dev libgconf2-dev libnss3 libasound2-dev libgtk2.0-0
sudo npm --unsafe-perm  install webtorrent-hybrid -g
export DISPLAY="0:99"
Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &

#wget http://site.tld/movies/Ex%20Machina%20%282015%29/Ex.Machina.2015.720p.BluRay.x264.YIFY.mp4
#webtorrent-hybrid --keep-seeding 'magnet:?xt=urn:btih:acf4ecfd065a23cb0b3eb4dcf1185e78686cad8a&dn=Ex.Machina.2015.720p.BluRay.x264.YIFY.mp4&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com'
