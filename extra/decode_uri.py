#!/usr/bin/env python3
import sys
import os
import argparse

import zlib
import urllib.parse

exec(open("../app/encode.py").read())

def parser():
    parser = argparse.ArgumentParser(description="encode uri helper")
    parser.add_argument('URI', nargs=1, help="URI to encode")
    return parser.parse_args()

### copied from ../app/utils.py ########################################
def decode_uri(encoded_uri):
    encoded_uri = urllib.parse.unquote_plus(encoded_uri)
    uri         = zlib.decompress(b64_decode(encoded_uri)).decode()
    return uri
########################################################################

def main():
    args = parser()
    uri  = args.URI[0]
    print(decode_uri(uri))

if __name__ == "__main__":
    main()
