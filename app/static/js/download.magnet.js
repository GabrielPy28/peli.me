//var torrentId = 'magnet:?xt=urn:btih:c9e15763f722f23e98a29decdfae341b98d53056&dn=Cosmos+Laundromat&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fcosmos-laundromat.torrent'

var client = new WebTorrent()

// HTML elements
var $body          = document.body
var $progressPerc  = document.querySelector('#progressPerc')
var $numPeers      = document.querySelector('#numPeers')
var $uploadSpeed   = document.querySelector('#uploadSpeed')
var $downloadSpeed = document.querySelector('#downloadSpeed')

// Download the torrent
console.log(torrentId)
client.add(torrentId, function (torrent) {

// Torrents can contain many files. Let's use the .mp4 file
  var file = torrent.files.find(function (file) {
    console.log(file)
    return file.name.endsWith('.mp4')
  })

  //console.log(file)
  //download the file in the browser
  file.getBlobURL(function(err, url) {
      if (err) return console.log(err)
      var a         = document.createElement('a')
      a.target      = '_blank'
      a.download    = file.name
      a.id          = "download"
      a.href        = url
      a.textContent = file.name
      document.documentElement.innerHTML = "";
      document.body.appendChild(a)
      document.getElementById("download").click()
   })

  // Trigger statistics refresh
  torrent.on('done', onDone)
  setInterval(onProgress, 500)
  onProgress()

  // Statistics
  function onProgress () {
    $progressPerc.innerHTML = (torrent.progress * 100).toFixed(2) + " %"

    $numPeers.innerHTML = torrent.numPeers + (torrent.numPeers === 1 ? ' nodo' : ' nodos')

    $downloadSpeed.innerHTML = prettyBytes(torrent.downloadSpeed) + '/s'
    $uploadSpeed.innerHTML   = prettyBytes(torrent.uploadSpeed)   + '/s'
  }

  function onDone () {
    $body.className += ' is-seed'
    onProgress()
  }

})

// Human readable bytes util
function prettyBytes(num) {
  var exponent, unit, neg = num < 0, units = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  if (neg) num = -num
  if (num < 1) return (neg ? '-' : '') + num + ' b'
  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1)
  num = Number((num / Math.pow(1000, exponent)).toFixed(2))
  unit = units[exponent]
  return (neg ? '-' : '') + num + ' ' + unit
}
