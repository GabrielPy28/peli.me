from app         import app, db, bcrypt
from app.mail    import send_notification_email
from app.utils   import encode_uri, parse_provider

from flask       import request
from hashlib     import md5
from datetime    import datetime
from flask_login import current_user

import random

class User(db.Document):
    username  = db.StringField(required=True, unique=True)
    email     = db.StringField(required=True, unique=True)
    about_me  = db.StringField(default='Ser humano moderado')
    phrase    = db.StringField(default='"Ellos mandan hoy porque tú obedeces" - A. Camus')
    password  = db.StringField(required=True); password_hashed = db.BooleanField(default=False)
    liked     = db.ListField(db.ReferenceField('Media'))
    profile   = db.StringField(required=True, default="user") #user|mod|admin

    status    = db.ListField(db.DictField(), required=True, default=[{
                    'state'      : 'ACTIVE', #'SILENCED|SUSPENDED'
                    'modered_by' : None,     # db.ReferenceField('User')
                    'date'       : datetime.now(),
                    'acked'      : False,
                }])

    #can be calculated from the status array but saved here for performance
    silenced_times  = db.IntField(required=True, default=0)
    suspended_times = db.IntField(required=True, default=0)

    confirmed     = db.BooleanField(default=False)
    confirmed_on  = db.DateTimeField()
    registered_on = db.DateTimeField(default=datetime.now)
    last_seen     = db.DateTimeField(default=datetime.now)

    def clean(self):
        #clean will be called on .save()
        #you can do whatever you want to clean data before saving

        #workaround for already hashed password, mongoengine makes difficult to
        #override the __init__ constructor:
        #https://stackoverflow.com/questions/16881624/mongoengine-0-8-0-breaks-my-custom-setter-property-in-models
        #http://docs.mongoengine.org/guide/document-instances.html#pre-save-data-validation-and-cleaning
        if not self.password_hashed:
            self.password        = bcrypt.generate_password_hash(self.password).decode('utf-8')
            self.password_hashed = True

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def reset_password(self, password):
        self.password        = bcrypt.generate_password_hash(password).decode('utf-8')
        self.password_hashed = True
        return self

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % (md5(self.email.encode('utf-8')).hexdigest(), size)

    def chatbro_signature(self):
        #http://example.chatbro.com / chat.html.j2
        signature  = request.headers['Host'].split(":")[0] #siteDomain
        signature += self.username              #siteUserExternalId
        signature += self.username              #siteUserFullName
        signature += self.avatar(50)            #siteUserAvatarUrl
        signature += "/perfil/" + self.username #siteUserProfileUrl
        signature += app.config['CHATBRO_ANTISPOOFING_KEY']

        return md5(signature.encode('utf-8')).hexdigest()

    def set_status(self, state):
        if state == "ACTIVE"    or \
           state == "SILENCED"  or \
           state == "SUSPENDED":

            status = {
                'state'      : state,
                #TODO 09-08-2018 19:04 >> how to save by ObjectId?
                'modered_by' : current_user.to_dbref(),
                'date'       : datetime.now(),
                'acked'      : False,
            }

            self.status.append(status)

            if   state == "SILENCED":
                self.silenced_times  += 1
            elif state == "SUSPENDED":
                self.suspended_times += 1

        self.save()

        return self

    def is_silenced(self):
        if self.status[-1]['state'] == "SILENCED":
            return True
        else:
            return False

    def silence(self):
        self.set_status("SILENCED")

    def unsilence(self):
        self.set_status("ACTIVE")

    def is_suspended(self):
        if self.status[-1]['state'] == "SUSPENDED":
            return True
        else:
            return False

    def suspend(self):
        self.set_status("SUSPENDED")

    def unsuspend(self):
        self.set_status("ACTIVE")

    def is_admin(self):
        if self.profile == "admin":
            return True
        else:
            return False

    def is_mod(self):
        if self.profile == "mod" or self.profile == "admin":
            return True
        else:
            return False

    def member_since(self):
        month = self.confirmed_on.month
        year  = self.confirmed_on.year

        months_spanish = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
           10: "Octubre",
           11: "Noviembre",
           12: "Diciembre",
        }

        return '%s %d' % (months_spanish[month], year)

    def last_seen_since(self):
        mins  = self.last_seen.minute
        hour  = self.last_seen.hour

        day   = self.last_seen.day
        month = self.last_seen.month
        year  = self.last_seen.year

        months_spanish = {
            1: "Enero",
            2: "Febrero",
            3: "Marzo",
            4: "Abril",
            5: "Mayo",
            6: "Junio",
            7: "Julio",
            8: "Agosto",
            9: "Septiembre",
           10: "Octubre",
           11: "Noviembre",
           12: "Diciembre",
        }

        return '%d de %s %d, %s:%s' % (day, months_spanish[month], year, hour, mins)

    def favorite(self, media):
        if not self.has_favorite(media):
            self.liked.append(media)
        return self

    def unfavorite(self, media):
        if self.has_favorite(media):
            self.liked.remove(media)
        return self

    def has_favorite(self, media):
        return self.liked.count(media) > 0

    def favorites(self):
        return self.liked

    def favorite_movies(self):
        movies = []

        for media in self.liked:
            if media.media_type == "peli":
                movies.append(media)

        return movies

    def favorite_series(self):
        movies = []

        for media in self.liked:
            if media.media_type == "serie":
                movies.append(media)

        return movies

    def favorite_animes(self):
        animes = []

        for media in self.liked:
            if media.media_type == "anime":
                animes.append(media)

        return animes

    def favorite_documentales(self):
        movies = []

        for media in self.liked:
            if media.media_type == "documental":
                movies.append(media)

        return movies

    def uploads(self, media_type=None, status=None):
        if media_type == None:
            if status == None:
                uploads = Upload.objects(uploader=self)
            else:
                uploads = Upload.objects(uploader=self, status=status)
        else:

            if   media_type == "pelis":        media_type = "peli"
            elif media_type == "series":       media_type = "serie"
            elif media_type == "animes":       media_type = "anime"
            elif media_type == "documentales": media_type = "documental"

            media   = Media.objects(media_type=media_type)
            uploads = self.uploads()
            uploads = uploads.filter(media__in=media)

        return uploads

    def uploaded_media_len(self, media):
        if len(media) <= 0:
            return '0'
        elif len(media) > 100:
            return '100'
        else:
            return '%d' % (len(media))

    def uploaded_movies_len(self):
        movies = self.uploads(media_type="peli", status="ACTIVE")
        return self.uploaded_media_len(movies)

    def uploaded_series_len(self):
        series = self.uploads(media_type="serie", status="ACTIVE")
        return self.uploaded_media_len(series)

    def uploaded_animes_len(self):
        animes = self.uploads(media_type="anime", status="ACTIVE")
        return self.uploaded_media_len(animes)

    def uploaded_documentales_len(self):
        documentales = self.uploads(media_type="documental", status="ACTIVE")
        return self.uploaded_media_len(documentales)

    def notifications(self, seen="new"):
        if seen == "new":
            return Notification.objects(user=self, seen=False).order_by('-date')
        if seen == "all":
            return Notification.objects(user=self).order_by('-date')

    def notify(self, msg, severity="info", email=False):
        if msg is None:
            return False
        else:
            new_notification = Notification(
                user=self,
                severity=severity,
                msg=msg,
                ).save()
            if email:
                send_notification_email(new_notification)
            return True

    @property
    def is_confirmed(self):
        return self.confirmed

    def __repr__(self):
        return '<User %r>' % (self.username)

    #required for flask-login
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        if self.status[-1]['state'] == "SUSPENDED":
            return False
        else:
            return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)
    #finish flask-login requirements

    meta = {
        'strict': True,
    }

class Notification(db.Document):
    user     = db.ReferenceField('User', required=True)
    severity = db.StringField(default="info") #error|warning|info
    msg      = db.StringField(required=True)
    date     = db.DateTimeField(default=datetime.now)
    seen     = db.BooleanField(default=False)

    hash     = db.StringField(required=True, unique=True)

    def clean(self):
        hash = self.user.username + \
               str(self.severity) + \
               str(self.msg)      + \
               str(self.date)

        self.hash = md5(hash.encode('utf-8')).hexdigest()

    def __repr__(self):
        return '<Notification %r>' % (self.id)

class Media(db.Document):

    moviedb_uri   = db.StringField(required=True)
    media_type    = db.StringField(required=True, choices=(
                     'peli',
                     'serie',
                     'anime',
                     'documental',
                   ))

    title         = db.StringField(required=True)
    title_unique  = db.StringField(required=True, unique=True)
    description   = db.StringField(required=True)
    release_date  = db.DateTimeField(required=True)
    score         = db.FloatField(required=True, default=0.0)
    poster_uri    = db.StringField(required=True)
    poster_b64    = db.StringField(required=True)
    fposter_uri   = db.StringField(required=True)
    fposter_b64   = db.StringField(required=True)
    bposter_uri   = db.StringField(required=True)
    bposter_b64   = db.StringField(required=True)
    lposter_uri   = db.StringField(required=True)
    lposter_b64   = db.StringField(required=True)
    genres        = db.ListField(db.StringField(), required=True)
    status        = db.StringField(required=True)
    counter       = db.FloatField(required=True)

    def clean(self):
        latest_media = Media.objects[:1].order_by('-counter')
        if latest_media:
            self.counter = latest_media[0].counter + 1.0
        else:
            self.counter = 0.0

    def hash(self):
        hash = str(self.id)
        return '%s' % (md5(hash.encode('utf-8')).hexdigest())

    def random(self):
        medias = Media.objects()
        random_idx = random.randrange(0, medias.count())
        return medias[random_idx]

    def has_published_uploads(self):
        if Upload.objects(media=self, status="ACTIVE").first():
            return True
        else:
            return False

    def download_uri(self, episode=None):
        if episode:
            upload = Upload.objects(media=self, episode=episode, status="ACTIVE").first()
        else:
            upload = Upload.objects(media=self, status="ACTIVE").first()

        if upload:
            return upload.download_uri
        else:
            return ''

    def uri(self, episode=None):
        if episode:
            upload = Upload.objects(media=self, episode=episode, status="ACTIVE").first()
        else:
            upload = Upload.objects(media=self, status="ACTIVE").first()

        if upload:
            return upload.uri
        else:
            return ''

    def encode(self, text):
        return encode_uri(text)

    def __repr__(self):
        return '<Media %r>' % (self.title_unique)

    # http://docs.mongoengine.org/guide/text-indexes.html
    meta = { #enable search functionality
        'indexes':
        [
            'counter',
            'score',
            {
                'fields':  ['$title', "$description"],
                'default_language': 'spanish',
                'weights': {'title': 10, 'description': 2}
            }
        ]
    }

class Episode(db.Document):
    media        = db.ReferenceField('Media', required=True)
    season       = db.IntField(required=True)
    chapter      = db.IntField(required=True)
    title        = db.StringField(required=True)
    description  = db.StringField(required=True)
    release_date = db.DateTimeField(required=True)
    score        = db.FloatField(required=True, default=0.0)
    poster_uri   = db.StringField(required=True)
    poster_b64   = db.StringField(required=True)
    fposter_uri  = db.StringField(required=True)
    fposter_b64  = db.StringField(required=True)

    hash         = db.StringField(required=True, unique=True)

    def clean(self):
        hash = self.media.title_unique + \
               str(self.season)        + \
               str(self.chapter)

        self.hash = md5(hash.encode('utf-8')).hexdigest()

    def next(self):
        next = None

        for episode in Episode.objects(media=self.media, season=self.season).order_by('chapter'):
            if episode.chapter > self.chapter:
                next = episode
                break

        if not next:
            for episode in Episode.objects(media=self.media, season=self.season+1).order_by('chapter'):
                next = episode
                break

        if not next:
            for episode in Episode.objects(media=self.media, season=1).order_by('chapter'):
                next = episode
                break

        return next

    def download_uri(self):
        upload = Upload.objects(media=self.media, episode=self, status="ACTIVE").first()

        if upload:
            return upload.download_uri
        else:
            return ''

    def __repr__(self):
        return '<Episode %r, season %r, chapter %r>' % \
            (self.media.title_unique, self.season, self.chapter)

class Upload(db.Document):
    media        = db.ReferenceField('Media',   required=True)
    episode      = db.ReferenceField('Episode') #required for serie|anime|documental
    uploader     = db.ReferenceField('User',    required=True)
    audio        = db.StringField(required=True, choices=(
                     'LATAM',
                     'ES',
                     'EN',
                   ))
    subtitle     = db.StringField(required=True, choices=(
                     'NINGUNO',
                     'ES',
                     'EN',
                   ))
    uri          = db.StringField(required=True)
    download_uri = db.StringField()
    download_uri_timestamp = db.DateTimeField()
    quality      = db.StringField(required=True, choices=(
                     'CAMARA', 'SD', 'HD', 'FULL_HD',
                   ))

    hash         = db.StringField(required=True)

    strikes      = db.ListField(db.ReferenceField('User'))
    status       = db.StringField(required=True, default="PREVIEW", choices=(
                     'PREVIEW', 'REPORTED', 'DCMA', 'ACTIVE'
                   ))

    def provider(self):
        return parse_provider(self.uri)

    def clean(self):
        if self.media.media_type == "peli":
            hash = self.media.title_unique + \
                   self.uploader.username  + \
                   self.audio              + \
                   self.uri                + \
                   self.quality
        else:
            hash = self.media.title_unique   + \
                   str(self.episode.season)  + \
                   str(self.episode.chapter) + \
                   self.uploader.username    + \
                   self.audio                + \
                   self.uri                  + \
                   self.quality

        self.hash = md5(hash.encode('utf-8')).hexdigest()

    def strike(self, user):
        return self.strikes.count(user) > 0

    def encode(self, text):
        return encode_uri(text)

    def __repr__(self):
        return '<Upload %r>' % (self.id)

class Dcma(db.Document):
    kind         = db.StringField(required=True, choices=(
                     'PROPIETARIO',
                     'AGENTE',
                   ))

    name         = db.StringField(required=True)
    email        = db.StringField(required=True)
    phone_number = db.StringField(required=True)
    organization = db.StringField(required=True)
    position     = db.StringField(required=True)
    address      = db.StringField(required=True)
    zipcode      = db.StringField(required=True)

    rightholder_name    = db.StringField(required=True)
    rightholder_country = db.StringField(required=True)

    local_uri    = db.StringField(required=True)
    remote_uri   = db.StringField(required=True)
    reported_on  = db.DateTimeField(default=datetime.now)
    confirmed    = db.BooleanField(default=False)
    confirmed_on = db.DateTimeField()

    def __repr__(self):
        return '<DCMA %r.%s.%s>' % (self.id, self.local_uri, self.remote_uri)
