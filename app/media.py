from app            import app
from app.cdn        import cdn_cache, cdn_delete
from app.utils      import parse_provider
from app.models     import Media, Episode, Upload
from app.decorators import threaded

from base64         import b64encode
from datetime       import datetime

from flask          import flash

import tmdbsimple as tmdb
import re, requests, unicodedata, string, magic, tempfile, os

tmdb.API_KEY = app.config['TMDB_API']
timeout = 3

def get_poster_uri(poster_size, sufix_path):
    if poster_size == "150x225":
        if sufix_path:
            uri = "https://image.tmdb.org/t/p/w150_and_h225_bestv2" + sufix_path
        else:
            uri = app.config['APP_URL'] + "/static/img/placeholders/150x225.png"
    elif poster_size == "300x450":
        if sufix_path:
            uri = "https://image.tmdb.org/t/p/w300_and_h450_bestv2" + sufix_path
        else:
            uri = app.config['APP_URL'] + "/static/img/placeholders/300x450.png"
    elif poster_size == "500x281":
        if sufix_path:
            uri = "https://image.tmdb.org/t/p/w500" + sufix_path
        else:
            uri = app.config['APP_URL'] + "/static/img/placeholders/500x281.png"
    elif poster_size == "1400x450":
        if sufix_path:
            uri = "https://image.tmdb.org/t/p/w1400_and_h450_face"  + sufix_path
        else:
            uri = app.config['APP_URL'] + "/static/img/placeholders/1400x450.png"
    return uri

def get_release_date(release_date):
    if not release_date:
        release_date = datetime.date(2000, 1, 1)
    return release_date

def get_poster_b64(poster_uri):
    poster_r = requests.get(poster_uri, verify=False, timeout=timeout)

    if isinstance(poster_r.content, str):
        poster_b64 = b64encode(poster_r.content)
    else:
        poster_b64 = str(b64encode(poster_r.content), 'utf-8')
    return poster_b64

def get_movie_info(moviedb_uri):
    movie = Media.objects(moviedb_uri=moviedb_uri).first()

    if movie is None:
        movie_id    = parse_tmdb_id(moviedb_uri)
        movie       = tmdb.Movies(movie_id)
        movie_info  = movie.info(language='es')
        title       = movie_info['title']

        poster_uri  = get_poster_uri('500x281', movie_info['backdrop_path'])
        poster_b64  = get_poster_b64(poster_uri)

        fposter_uri = get_poster_uri('300x450', movie_info['poster_path'])
        fposter_b64 = get_poster_b64(fposter_uri)

        bposter_uri = get_poster_uri('1400x450', movie_info['backdrop_path'])
        bposter_b64 = get_poster_b64(bposter_uri)

        lposter_uri = get_poster_uri('150x225', movie_info['poster_path'])
        lposter_b64 = get_poster_b64(lposter_uri)

        genres = ['all']
        for genre in movie_info['genres']:
            genres.append(genre['name'])

        release_date = get_release_date(movie_info['release_date'])

        new_movie   = Media(
                        moviedb_uri   = moviedb_uri,
                        media_type    = "peli",
                        title         = title,
                        title_unique  = get_title_unique(title),
                        description   = movie_info['overview'],
                        release_date  = release_date,
                        score         = float(movie_info['vote_average']) * 10,
                        poster_uri    = poster_uri,
                        poster_b64    = poster_b64,
                        fposter_uri   = fposter_uri,
                        fposter_b64   = fposter_b64,
                        bposter_uri   = bposter_uri,
                        bposter_b64   = bposter_b64,
                        lposter_uri   = lposter_uri,
                        lposter_b64   = lposter_b64,
                        genres        = genres,
                        status        = movie_info['status'],
                     ).save()

        app.logger.debug('NEW MOVIE, VIA TMDB API: "{}", title: "{}"'.format(moviedb_uri, title))

def upload_movie_link(moviedb_uri, uri, quality, audio, subtitle, user):
    movie        = Media.objects(moviedb_uri=moviedb_uri).first()
    download_uri = verify_link(moviedb_uri, uri, user)

    upload       = Upload.objects(uri=uri).first()

    if upload is None:
        new_upload = Upload(
                        media        = movie,
                        uploader     = user,
                        audio        = audio,
                        subtitle     = subtitle,
                        uri          = uri,
                        download_uri = download_uri,
                        download_uri_timestamp = datetime.now(),
                        quality      = quality,
                     ).save()

        app.logger.debug(
            'NEW UPLOAD, title: "{}", uploader: "{}", '
            'audio: "{}", subtitle: "{}", uri: "{}", download_uri: "{}", quality: "{}"'
            .format(movie.title_unique, user.username, audio, subtitle, uri, download_uri, quality))
    else:
        msg = ("Ops!, el link <a href='{}'>{}</a>"
               ", de la película <a href='{}'>{}</a>"
               " ya se compartió, por favor intente de nuevo")
        msg = msg.format(uri, uri, "/peli/"+upload.media.title_unique, upload.media.title)
        user.notify(msg, severity="warning")
        app.logger.debug(
            'UPLOAD ALREADY EXISTS, title: "{}", uploader: "{}", audio: "{}", subtitle: "{}", uri: "{}", quality: "{}"'
            .format(movie.title_unique, user.username, audio, subtitle, uri, quality))

def edit_movie_link(moviedb_uri, upload_hash, uri, download_uri, quality, audio, subtitle, user):
    upload       = Upload.objects(hash=upload_hash).first()
    media        = upload.media

    if upload.uri != uri and upload.download_uri == download_uri:
        download_uri = verify_link(moviedb_uri, uri, user)

    upload.media = Media.objects(moviedb_uri=moviedb_uri).first()

    if upload.uri != uri:
        cdn_delete(upload.uri)
        upload.uri = uri

    if upload.download_uri != download_uri:
        cdn_delete(upload.uri)
        upload.download_uri = download_uri
        upload.download_uri_timestamp = datetime.now()

    upload.quality      = quality
    upload.audio        = audio
    upload.subtitle     = subtitle
    upload.user         = user
    upload.save()

    delete_media_leftovers(media)

def get_serie_info(moviedb_uri, media_type="serie"):
    serie = Media.objects(moviedb_uri=moviedb_uri).first()

    if serie is None:
        serie_id    = parse_tmdb_id(moviedb_uri)
        serie       = tmdb.TV(serie_id)
        serie_info  = serie.info(language='es')

        title       = serie_info['name']

        poster_uri  = get_poster_uri('500x281', serie_info['backdrop_path'])
        poster_b64  = get_poster_b64(poster_uri)

        fposter_uri = get_poster_uri('300x450', serie_info['poster_path'])
        fposter_b64 = get_poster_b64(fposter_uri)

        bposter_uri = get_poster_uri('1400x450', serie_info['backdrop_path'])
        bposter_b64 = get_poster_b64(bposter_uri)

        lposter_uri = get_poster_uri('150x225', serie_info['poster_path'])
        lposter_b64 = get_poster_b64(lposter_uri)

        genres = ['all']
        for genre in serie_info['genres']:
            genres.append(genre['name'])

        new_serie   = Media(
                        moviedb_uri   = moviedb_uri,
                        media_type    = media_type,
                        title         = title,
                        title_unique  = get_title_unique(title),
                        description   = serie_info['overview'],
                        release_date  = serie_info['first_air_date'],
                        score         = float(serie_info['vote_average']) * 10,
                        poster_uri    = poster_uri,
                        poster_b64    = poster_b64,
                        fposter_uri   = fposter_uri,
                        fposter_b64   = fposter_b64,
                        bposter_uri   = bposter_uri,
                        bposter_b64   = bposter_b64,
                        lposter_uri   = lposter_uri,
                        lposter_b64   = lposter_b64,
                        genres        = genres,
                        status        = serie_info['status'],
                     ).save()

        app.logger.debug('NEW SERIE, VIA TMDB API: "{}", title: "{}"'.format(moviedb_uri, title))

def get_chapter_info(moviedb_uri, season, chapter):
    serie   = Media.objects(moviedb_uri=moviedb_uri).first()
    episode = Episode.objects(media=serie, season=season, chapter=chapter).first()

    if episode is None:
        serie_id = parse_tmdb_id(moviedb_uri)
        episode  = tmdb.TV_Episodes(serie_id, season, chapter)

        episode_info = episode.info(language='es')

        title = episode_info['name']

        if episode_info['still_path']:
            poster_uri  = "https://image.tmdb.org/t/p/w150_and_h225_bestv2/" + episode_info['still_path']
            fposter_uri = "https://image.tmdb.org/t/p/w300_and_h450_bestv2"  + episode_info['still_path']
        else: #if there is no poster for this specific chapter, use serie's one
            poster_uri  = serie.poster_uri
            fposter_uri = serie.fposter_uri

        poster_b64  = get_poster_b64(poster_uri)
        fposter_b64 = get_poster_b64(fposter_uri)

        new_episode = Episode(
                        media   = serie,
                        season  = season,
                        chapter = chapter,
                        title   = title,
                        description  = episode_info['overview'],
                        release_date = episode_info['air_date'],
                        score        = float(episode_info['vote_average']) * 10,
                        poster_uri   = poster_uri,
                        poster_b64   = poster_b64,
                        fposter_uri  = fposter_uri,
                        fposter_b64  = fposter_b64,
                     ).save()

        msg = 'NEW EPISODE, VIA TMDB API: "{}", season: "{}", chapter: "{}", title: "{}"'
        app.logger.debug(msg.format(serie.title_unique, season, chapter, title))
    else:
        msg = 'EPISODE ALREADY EXISTS, "{}", season: "{}", chapter: "{}", title: "{}"'
        app.logger.debug(msg.format(serie.title_unique, episode.season, episode.chapter, episode.title))

def upload_serie_link(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter):
    serie        = Media.objects(moviedb_uri=moviedb_uri).first()
    episode      = Episode.objects(media=serie, season=season, chapter=chapter).first()
    download_uri = verify_link(moviedb_uri, uri, user)

    upload       = Upload.objects(uri= uri).first()

    if upload is None:
        new_upload = Upload(
                        media        = serie,
                        episode      = episode,
                        uploader     = user,
                        audio        = audio,
                        subtitle     = subtitle,
                        uri          = uri,
                        download_uri = download_uri,
                        download_uri_timestamp = datetime.now(),
                        quality      = quality,
                     ).save()

        msg = 'NEW UPLOAD, title: "{}", episode: "{}", uploader: "{}", ' \
            'audio: "{}", subtitle: "{}", uri: "{}", download_uri: "{}", quality: "{}"'
        app.logger.debug(msg.format(
            serie.title_unique, episode.title, user.username,
            audio, subtitle, uri, download_uri, quality))
    else:
        msg = ("Ops!, el link <a href='{}'>{}</a>"
               ", de <a href='{}'>{}</a>"
               " temporada {}, capítulo {}"
               ", ya se compartió, por favor intente de nuevo")
        msg = msg.format(uri, uri,
                         "/peli/"+upload.media.title_unique, upload.media.title,
                         upload.episode.season, upload.season.chapter)
        user.notify(msg, severity="warning")
        msg = 'UPLOAD ALREADY EXISTS, title: "{}", episode: "{}", uploader: "{}", ' \
                'audio: "{}", subtitle: "{}", uri: "{}", quality: "{}"'
        app.logger.debug(msg.format(
            serie.title_unique, episode.title, user.username,
            audio, subtitle, uri, quality))

def edit_serie_link(moviedb_uri, upload_hash, uri, download_uri, quality, audio, subtitle, user, season, chapter):
    upload       = Upload.objects(hash=upload_hash).first()
    media        = upload.media
    episode      = upload.episode

    if upload.uri != uri and upload.download_uri == download_uri:
        download_uri = verify_link(moviedb_uri, uri, user)

    upload.media   = Media.objects(moviedb_uri=moviedb_uri).first()
    upload.episode = Episode.objects(media=upload.media, season=season, chapter=chapter).first()

    if upload.uri != uri:
        cdn_delete(upload.uri)
        upload.uri = uri

    if upload.download_uri != download_uri:
        cdn_delete(upload.uri)
        upload.download_uri = download_uri
        upload.download_uri_timestamp = datetime.now()

    upload.quality      = quality
    upload.audio        = audio
    upload.subtitle     = subtitle
    upload.user         = user
    upload.save()

    delete_media_leftovers(media, episode)

@threaded
def add_movie(moviedb_uri, uri, quality, audio, subtitle, user):
    with app.app_context():
        try:
            get_movie_info(moviedb_uri)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="warning")
            raise

        try:
            upload_movie_link(moviedb_uri, uri, quality, audio, subtitle, user)
        except Exception as e:
            media = Media.objects(moviedb_uri=moviedb_uri).first()
            delete_media_leftovers(media)
            raise

@threaded
def edit_movie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user):
    with app.app_context():
        try:
            get_movie_info(moviedb_uri)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="error")
            raise

        edit_movie_link(moviedb_uri, upload_hash, uri, download_uri, quality, audio, subtitle, user)

@threaded
def add_serie(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter):
    with app.app_context():
        try:
            get_serie_info(moviedb_uri)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="warning")
            raise

        try:
            get_chapter_info(moviedb_uri, season, chapter)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo episodio " + \
                  "con la TMDB URL => {}, temporada {}, episodio {}, Error: {}"\
                  .format(moviedb_uri, season, chapter, str(e))
            user.notify(msg, severity="error")
            media = Media.objects(moviedb_uri=moviedb_uri).first()
            delete_media_leftovers(media)
            raise

        try:
            upload_serie_link(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
        except Exception as e:
            media   = Media.objects(moviedb_uri=moviedb_uri).first()
            episode = Episode.objects(media=media, season=season, chapter=chapter).first()
            delete_media_leftovers(media, episode)
            raise

def add_anime(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter):
    with app.app_context():
        try:
            #TMDB api uses same methods for serie|anime|documental
            get_serie_info(moviedb_uri, "anime")
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="error")
            raise

        try:
            get_chapter_info(moviedb_uri, season, chapter)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo episodio " + \
                  "con la TMDB URL => {}, temporada {}, episodio {}, Error: {}"\
                  .format(moviedb_uri, season, chapter, str(e))
            user.notify(msg, severity="error")
            media = Media.objects(moviedb_uri=moviedb_uri).first()
            delete_media_leftovers(media)
            raise

        try:
            upload_serie_link(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
        except Exception as e:
            media   = Media.objects(moviedb_uri=moviedb_uri).first()
            episode = Episode.objects(media=media, season=season, chapter=chapter).first()
            delete_media_leftovers(media, episode)
            raise

def add_documental(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter):
    with app.app_context():
        try:
            #TMDB api uses same methods for serie|anime|documental
            get_serie_info(moviedb_uri, "documental")
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="error")
            raise

        try:
            get_chapter_info(moviedb_uri, season, chapter)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo episodio " + \
                  "con la TMDB URL => {}, temporada {}, episodio {}, Error: {}"\
                  .format(moviedb_uri, season, chapter, str(e))
            user.notify(msg, severity="error")
            media = Media.objects(moviedb_uri=moviedb_uri).first()
            delete_media_leftovers(media)
            raise

        try:
            upload_serie_link(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
        except Exception as e:
            media   = Media.objects(moviedb_uri=moviedb_uri).first()
            episode = Episode.objects(media=media, season=season, chapter=chapter).first()
            delete_media_leftovers(media, episode)
            raise

@threaded
def edit_serie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter):
    with app.app_context():
        try:
            get_movie_info(moviedb_uri)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Error: {}".format(moviedb_uri, str(e))
            user.notify(msg, severity="error")
            raise

        try:
            get_chapter_info(moviedb_uri, season, chapter)
        except Exception as e:
            e = re.sub(r' for url.*', '', str(e))
            msg = "No se pudo procesar la petición de nuevo episodio " + \
                  "con la TMDB URL => {}, temporada {}, episodio {}, Error: {}"\
                  .format(moviedb_uri, season, chapter, str(e))
            user.notify(msg, severity="error")
            media = Media.objects(moviedb_uri=moviedb_uri).first()
            delete_media_leftovers(media)
            raise

        edit_serie_link(moviedb_uri, upload_hash, uri, download_uri, quality, audio, subtitle, user, season, chapter)

def edit_anime(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter):
    #TMDB api uses same methods for serie|anime|documental
    edit_serie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter)

def edit_documental(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter):
    #TMDB api uses same methods for serie|anime|documental
    edit_serie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter)

def parse_tmdb_id(tmdb_uri):
    # tmdb_uri = "https://www.themoviedb.org/movie/10138-iron-man-2"
    # tmdb_uri = "https://www.themoviedb.org/tv/1396-breaking-bad"
    regex = r"[movie|tv]/(\d+)-?"
    match = re.search(regex, tmdb_uri)
    if match:
        return match.groups()[0] #10138|1396
    return None

def get_title_unique(title):
    #replace á|é|í|... chars with its closest ascii relatives a|e|i|...
    title_unique = ''.join((c for c in unicodedata.normalize('NFD', title) if unicodedata.category(c) != 'Mn'))
    #remove special characters but whitespaces / -
    validchars   = string.ascii_letters + string.digits + ' ' + '-'
    title_unique = ''.join(c for c in title_unique if c in validchars)

    title_unique = ' '.join(title_unique.split()) #remove extra|trailing whitespaces
    title_unique = title_unique.replace(" ", "-")

    i = 0
    while Media.objects(title_unique=title_unique):
        i = i+1
        title_unique = title_unique + "-" + str(i)
        app.logger.debug("Ops, title_unique already exists: {}, trying with new one ...".format(title_unique))

        if i > 100:
            app.logger.debug("Couldn't find a new valid name, something is terrible wrong...")
            break

    return title_unique

def delete_media_leftovers(media, episode=None):
    if episode:
        other_uploads = Upload.objects(episode=episode).first()
        if other_uploads is None:
            episode.delete()

    other_uploads = Upload.objects(media=media).first()

    if other_uploads is None:
        Episode.objects(media=media).delete()
        media.delete()

def media_with_published_uploads(media_list):
    published = []
    for media in media_list:
        if media.has_published_uploads():
            published.append(media)
    return published

def get_download_uri(uri):
    if parse_provider(uri) == "MAGNET":
        # TODO 09-07-2018 10:31 >> find out method to verify magnet links
        download_uri = uri
    else:
        download_uri = cdn_cache(uri)
    return download_uri

def verify_link(moviedb_uri, uri, user):
    if app.config['APP_URL_PROVIDER_VERIFICATION']:
        try:
            download_uri = get_download_uri(uri)
            return download_uri
        except Exception as e:
            # remove sensitive data
            e = str(e).replace("\n", "");
            e = re.sub(r' for url.*', '', e);
            e = re.sub(r'RAN.*--get-url ', '', e)
            e = re.sub(r'STDOUT.*File', 'File', e)
            msg = "No se pudo procesar la petición de nuevo contenido " + \
                  "con la TMDB URL => {}, Proveedor => {}, Error: {}"     \
                  .format(moviedb_uri, parse_provider(uri), e)
            user.notify(msg, severity="error")
            raise
    else:
        return None

def validate_media_type(media_type):
    if   media_type == "pelis"  or media_type == "peli" : media_type = "peli"
    elif media_type == "series" or media_type == "serie": media_type = "serie"
    elif media_type == "animes" or media_type == "anime": media_type = "anime"
    elif media_type == "documentales" or media_type == "documental": media_type = "documental"
    else: return False

    return media_type

def validate_link(uri):
    provider = parse_provider(uri)

    if provider == "MAGNET":
        # uri = 'magnet:?xt=urn:btih:dd825...'
        regex = r"^magnet:\?xt=urn:btih:"
    elif provider == "YOUTUBE":
        # uri = 'https://www.youtube.com/watch?v=WjhmB0JlCTo'
        regex = r"^https://(www.)?youtube.com/watch\?v=[\w-]+$"
    elif provider == "YOUTU":
        # uri = 'https://youtu.be/WjhmB0JlCTo'
        regex = r"^https://youtu.be/[\w-]+$"
    elif provider == "VIMEO":
        # uri = 'https://vimeo.com/114802340'
        regex = r"^https://(www.)?vimeo.com/[\w-]+$"
    # elif provider == "GLORIA": #uses mp3u which breaks ffmpeg conversion
        # # uri = 'https://gloria.tv/video/87fR6B8TTimD1aJppjv8ffsia'
        # regex = r"^https://(www.)?gloria.tv/video/[\w-]+$"
    elif provider == "RAPIDVIDEO":
        # uri = 'https://www.rapidvideo.com/embed/FVZ1482V44'
        # uri = 'https://www.rapidvideo.com/e/FVZ1482V44&q=720p'
        # uri = 'https://www.rapidvideo.com/e/FVZ1482V44'
        regex = r"^https://(www.)?rapidvideo.com/(embed|e|v)/.*([\w&=])+"
    elif provider == "SULLCA":
        # uri = 'http://dragonball.sullca.com/videos/solidfiles/videovip.php?id=e9e6e7'
        regex = r"^http(s)?://(dragonball.)?sullca.com/videos/.*"
    # elif provider == "OPENLOAD":   #direct download restringed by ip
        # # uri = 'https://openload.co/embed/K_KIUHM9_nI'
        # # uri = 'https://openload.co/f/K_KIUHM9_nI/'
        # regex = r"^https://openload.co/(embed|f)/([\w_])+"
    # elif provider == "STREAMANGO": #direct download restringed by ip
        # # uri = 'https://streamango.com/embed/kbqnofonnfnotoed/Atomic_Blonde_SUB_mkv_mp4'
        # regex = r"^https://streamango.com/embed/([\w])+/"
    elif provider == "OK": #direct download restringed by ip
        # uri = 'https://ok.ru/videoembed/32808831501'
        regex = r"^http(s)?://ok.ru/videoembed/([\w])+"
    # elif provider == "VIDLOX" #unsupported
        # uri = 'https://vidlox.me/embed-dt656nca8ygd.html'
        # regex = r"^http(s)?://vidlox.me/v/([\w])+"
    # elif provider == "VIDCLOUD" #unsupported
        # uri = 'https://vidcloud.co/embed/5ba672dc02712'
        # regex = r"^http(s)?://vidcloud.co/v/([\w])+"
    # elif provider == "FEMBED" #unsupported
        # uri = 'https://www.fembed.com/v/05vl58k1n96'
        # regex = r"^http(s)?://www.fembed.com/v/([\w])+"
    # elif provider == "GAMOVIDEO": #unsupported
        # # uri = 'http://gamovideo.com/mi6qurimv00v'
        # regex = r"^http(s)?://gamovideo.com/([\w])+"
    else:
        return url_has_valid_mp4(uri)

    match = re.search(regex, uri)

    if match:
        return True
    else:
        return False

def validate_bulk_links(media_type, entries):
    if len(entries) > app.config['APP_MAX_UPLOADS_BULK']:
        flash('El formulario sobrepasa las {} líneas, '
              'por favor elimine algunas entradas y vuelva a intentarlo'.format(
                   app.config['APP_MAX_UPLOADS_BULK']))
        return False

    line = 1
    for entry in entries:

        if media_type == "peli":
            if len(entry.split()) != len("URL_TMDB URI CALIDAD AUDIO SUBTITULOS".split()):
                flash('Campos incompletos en línea {}, "{}"'.format(line, entry))
                return False
            moviedb_uri, uri, quality, audio, subtitle = entry.split()

        else:
            if len(entry.split()) != \
               len("URL_TMDB TEMPORADA CAPITULO URI CALIDAD AUDIO SUBTITULOS".split()):
                flash('Campos incompletos en línea {}, "{}"'.format(line, entry))
                return False
            moviedb_uri, season, chapter, uri, quality, audio, subtitle = entry.split()

        if quality != "FULL_HD" and \
           quality != "HD"      and \
           quality != "SD"      and \
           quality != "CAMARA":
            flash('CALIDAD incorrecta en línea {}, "{}" => "{}"'.format(line, entry, quality))
            return False

        if audio != "LATAM" and \
           audio != "ES"    and \
           audio != "EN":
            flash('AUDIO incorrecto en línea {}, "{}" => "{}"'.format(line, entry, audio))
            return False

        if subtitle != "NINGUNO" and \
           subtitle != "ES"      and \
           subtitle != "EN":
            flash('SUBTITULO incorrecto en línea {}, "{}" => "{}"'.format(line, entry, subtitle))
            return False

        line += 1

    return True

def url_has_valid_mp4(url):
    if url is None:
        return False

    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as tmpfile:
        temp_filename = tmpfile.name

    app.logger.debug(temp_filename)
    app.logger.debug(url)

    try:
        response = requests.get(url, stream=True, verify=False, timeout=timeout)
    except requests.exceptions.RequestException as e:
        app.logger.debug(e)
        return False

    f = open(temp_filename, "wb")
    for chunk in response.iter_content(chunk_size=32):
        if chunk: # filter out keep-alive new chunks
            f.write(chunk)
            f.close()
            response.close()
            break

    mime = magic.from_file(temp_filename, mime=True)
    os.remove(temp_filename)

    app.logger.debug(mime)
    return mime == "video/mp4"

@threaded
def refresh_download_link(upload):
    with app.app_context():
        try:
            upload.download_uri = get_download_uri(upload.uri)
            upload.download_uri_timestamp = datetime.now()
            upload.save()
        except Exception as e:
            # remove sensitive data
            e = str(e).replace("\n", "");
            e = re.sub(r' for url.*', '', e);
            e = re.sub(r'RAN.*--get-url ', '', e)
            e = re.sub(r'STDOUT.*File', 'File', e)
            app.logger.debug(e)
            raise
