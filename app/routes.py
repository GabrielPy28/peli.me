from flask       import render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user, login_user, logout_user

from app         import app, login_manager
from app.cdn     import cdn_delete
from app.mail    import APP_FROM, send_confirmation_email, send_email, send_reset_passwd_email
from app.mail    import send_dcma_confirmation_email
from app.forms   import LoginForm, RegisterForm, PasswordRecoverForm
from app.forms   import MetaDataForm, PasswordResetForm, DcmaForm
from app.forms   import MovieForm, SerieForm, AnimeForm, DocumentalForm
from app.forms   import MovieBulkForm, SerieBulkForm
from app.forms   import EditUserForm, DirectViewForm, DirectDownloadForm
from app.token   import generate_status_token, confirm_email_token
from app.token   import confirm_status_token,  confirm_dcma_token
from app.media   import parse_tmdb_id, validate_link, get_title_unique
from app.media   import validate_media_type, validate_bulk_links
from app.media   import add_movie,  add_serie,  add_anime,  add_documental
from app.media   import edit_movie, edit_serie
from app.media   import delete_media_leftovers, media_with_published_uploads
from app.media   import url_has_valid_mp4, refresh_download_link, get_poster_b64
from app.utils   import encode_uri, decode_uri, sanitazer_uri, parse_provider
from app.models  import User, Media, Episode, Upload, Notification, Dcma
from app.decorators import user_confirmed_required, user_mod_required, user_admin_required

from datetime    import datetime, timedelta
from collections import defaultdict
from mongoengine.queryset.visitor import Q

import time, re, os, random

MEDIA_TYPE_MAPPING = {
    "peli":       "pelis",
    "serie":      "series",
    "anime":      "animes",
    "documental": "documentales",
}

GENRE_MAPPING = {
    "acción":          ["Acción", "Action & Adventure", "Aventura"],
    "bélico":          ["Bélica", "War & Politics"],
    "ciencia ficción": ["Ciencia ficción", "Sci-Fi & Fantasy"],
    "comedia":         ["Comedia"],
    "crimen":          ["Crimen"],
    "drama":           ["Drama"],
    "infantil":        ["Kids", "Familia"],
    "misterio":        ["Misterio", "Suspense"],
    "músical":         ["Música"],
    "terror":          ["Terror", "Horror", "Thriller"],
}

PER_PAGE = app.config['APP_ITEMS_PER_PAGE']

@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk=user_id).first()

#http://flask.pocoo.org/docs/0.12/templating/#context-processors
@app.context_processor
def inject_global_template_variables():
    return dict(
        APP_URL             = app.config['APP_URL'],
        MAX_STRIKES         = app.config['APP_MAX_STRIKES'],
        MAX_UPLOADS_BUILK   = app.config['APP_MAX_UPLOADS_BULK'],
        FORWARD_REWARD_SECS = app.config['APP_FORWARD_REWARD_SECONDS'],
        CHATBRO_ID          = app.config['CHATBRO_ID'],
    )

@app.context_processor
def utility_processors():
    def footer_bg_image():
        image_directory = '/static/img/footers/'
        image_abspath   = os.path.abspath(os.path.dirname(__file__)) + image_directory
        images = []
        for file in os.listdir(image_abspath):
            if file.endswith(".jpg"):
                images.append(file)
        # app.logger.debug(images)
        image = random.choice(images)
        return image_directory + image

    return dict(
        footer_bg_image=footer_bg_image,
    )

# http://flask.pocoo.org/docs/0.12/templating/#registering-filters
@app.template_filter()
def encode(uri):
    return encode_uri(uri)

@app.route('/index.html')
def index_html():
    return redirect(url_for('index'))

@app.route('/')
@app.route('/index')
@app.route('/ultimas/')
@app.route('/ultimas/<int:page>')
@app.route('/ultimas/pag/<int:page>')
def index(page=1):
    #oldest to newest
    latest = Media.objects.order_by('-counter').paginate(page=page, per_page=PER_PAGE)
    latest.items = media_with_published_uploads(latest.items)
    return render_template("index.html.j2", media=latest)

@app.route('/populares/')
@app.route('/populares/<int:page>')
@app.route('/populares/pag/<int:page>')
def populares(page=1):
    populares = Media.objects.order_by('-score').paginate(page=page, per_page=PER_PAGE)
    populares.items = media_with_published_uploads(populares.items)
    return render_template("index.html.j2", subtitle="Lo más popular", media=populares)

@app.route('/en-cartelera/')
@app.route('/en-cartelera/<int:page>')
@app.route('/en-cartelera/pag/<int:page>')
def en_cartelera(page=1):
    #oldest to newest
    latest = Media.objects.order_by('-counter').paginate(page=page, per_page=PER_PAGE)
    latest.items = media_with_published_uploads(latest.items)

    #since TMDB doesn't provide a easy way to check for is_playing status
    #per movie, use 60 days as a raw daycount approximation
    datetime_cartelera = datetime.now() - timedelta(days=60)

    latest_in_cartelera = []
    for media in latest.items:
        if media.release_date > datetime_cartelera:
            latest_in_cartelera.append(media)

    latest.items = latest_in_cartelera
    return render_template("index.html.j2", subtitle="En cartelera", media=latest)

@app.route('/buscar', methods=['GET', 'POST'])
@app.route('/search', methods=['GET', 'POST'])
@app.route('/search/<int:page>', methods=['GET', 'POST'])
@app.route('/search/pag/<int:page>', methods=['GET', 'POST'])
def search(page=1):
    if request.method == 'GET':
        return redirect(url_for('index'))
    query  = request.form['query']
    #oldest to newest
    latest = Media.objects.search_text(query).order_by('-counter').paginate(page=page, per_page=PER_PAGE)
    latest.items = media_with_published_uploads(latest.items)
    return render_template("index.html.j2",
                           subtitle="Resultados de '{}':".format(query),
                           media=latest)

@app.route('/login',  methods=['GET', 'POST'])
@app.route('/entrar', methods=['GET', 'POST'])
@app.route('/login/<referrer>',  methods=['GET', 'POST'])
@app.route('/entrar/<referrer>', methods=['GET', 'POST'])
def login(referrer=None):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if request.method == 'POST' and form.validate():
        user = User.objects(email=form.email.data).first()
        if user is None:
            flash('El correo "{}" no se ha registrado'.format(form.email.data))
            return render_template('user_login.html.j2', title='Iniciar sesión', form=form)
        else:
            if user.check_password(form.password.data):
                login_user(user)
                user.last_seen = datetime.utcnow()
                user.save()

                if not current_user.is_active:
                    flash("Lo sentimos, su cuenta ha "
                          "sido temporalmente deshabilitada.")

                if referrer:
                    referrer = decode_uri(referrer)
                else:
                    referrer = request.referrer

                return redirect(referrer)
            else:
                flash('El usuario o contraseña es incorrecto')
                return render_template('user_login.html.j2', title='Iniciar sesión', form=form)
    return render_template('user_login.html.j2', title='Iniciar sesión', form=form)

@app.route('/logout')
@app.route('/salir')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register',    methods=['GET', 'POST'])
@app.route('/registrarse', methods=['GET', 'POST'])
def register():
    if not app.config['APP_REGISTER']:
        return render_template('user_register_disabled.html.j2')

    form = RegisterForm()
    if request.method == 'POST' and form.validate():
        user = User.objects(email=form.email.data).first()
        if user is None:
            user = User.objects(username=form.username.data).first()
            if user is None:
                if User.objects().first() is None:
                    profile="admin" #make 1st user admin
                else:
                    profile="user"
                new_user = User(
                            username=form.username.data,
                            email=form.email.data,
                            password=form.password.data,
                            profile=profile,
                            ).save()
                send_confirmation_email(new_user)
                login_user(new_user)
                return redirect(url_for('user_unconfirmed'))
            else:
                flash('El usuario {} ya existe, por favor vuelva a intentarlo'
                      .format(form.username.data))
        else:
            flash('El usuario con el correo {} ya existe'.format(form.email.data))
    return render_template('user_register.html.j2', title='Registrarse', form=form)

@app.route('/user/unconfirmed')
@app.route('/confirmacion/pendiente')
def user_unconfirmed():
    if current_user.confirmed:
        flash('La cuenta adjunta ({}) ya ha sido confirmada, '
              'puede iniciar sesión'.format(current_user.email))
        return redirect('login')
    return render_template('user_unconfirmed.html.j2')

@app.route('/confirmar/<item>/<token>')
def confirm(item, token):
    if   item == "correo":
        return confirm_email(token)
    elif item == "estado":
        return confirm_status(token)
    elif item == "dcma":
        return confirm_dcma(token)
    else:
        return render_template('404.html.j2'), 404

def confirm_email(token):
    email = confirm_email_token(token)
    if not email:
        flash('El código de confirmación es inválido o ha expirado')
        return redirect(url_for('register'))

    user = User.objects(email=email).first()
    if user.confirmed:
        flash('La cuenta adjunta ({}) ya ha sido confirmada, '
              'puede iniciar sesión'.format(email))
    else:
        user.confirmed    = True
        user.confirmed_on = datetime.now()
        user.save()
        flash('Ha confirmado exitosamente su cuenta ({})'.format(email))
    return redirect(url_for('index'))

def confirm_status(token):
    status = confirm_status_token(token)
    if not status:
        flash('El código de confirmación es inválido o ha expirado')
        return render_template('404.html.j2'), 404

    user = User.objects(username=status['username']).first()
    if user is None:
        flash('La cuenta especificada ({}) no existe'.format(status['username']))
        return redirect(url_for('register'))
    elif user.status[-1]['state'] != status['state']:
        flash('El código de confirmación es inválido o ha expirado')
        return render_template('404.html.j2'), 404
    else:
        if user.status[-1]['acked']:
            flash('Ya ha confirmado de recibido, '
                  'en breve un moderador retomará su caso')
        else:
            user.status[-1]['acked'] = True
            user.save()

            moderator = user.status[-1]['modered_by']
            status_es = { 'silenced':'silenciado', 'suspended':'suspendido' }
            msg = ("¡Hola!, <a href='{}'>{}</a> confirmó de recibido ({}),"
                   " por favor tome unos minutos para evaluar su perfil"
                   " y modificar su estado si lo considera pertinente,"
                   " gracias por hacer de <a href='{}'>{}</a> el mejor"
                   " sitio para ver contenido.")
            msg = msg.format(
                    url_for('profile', username=user.username), user.username,
                    status_es[status['state']],
                    app.config['APP_URL'], app.config['APP_URL'],
                  )
            moderator.notify(msg)

            msg = ("Gracias por confirmar de recibido, su nuevo estado"
                   " es \"{}\", en breve un moderador retomará su caso.")
            msg = msg.format(status_es[status['state']])
            user.notify(msg, email=True)
            flash(msg)

    if user.is_authenticated:
        return redirect(url_for('index'))
    else:
        return redirect(url_for('notifications'))

def confirm_dcma(token):
    dcma_id = confirm_dcma_token(token)
    if not dcma_id:
        flash('El código de confirmación es inválido o ha expirado')

    dcma = Dcma.objects(id=dcma_id).first()
    if dcma.confirmed:
        flash('El reporte de la URL "{}" ya ha sido confirmado'
              .format(dcma.remote_uri))
    else:
        dcma.confirmed    = True
        dcma.confirmed_on = datetime.now()
        dcma.save()

        upload = Upload.objects(uri=dcma.remote_uri).first()
        upload.status = "DCMA"
        upload.save()

        msg = ("Su link <a href='{}'>{}</a> de <a href='{}'>{}</a>"
                " recibió un reporte <a href='{}'>DCMA</a> de"
                " {} / {} por lo cual ha sido deshabilitado"
                " temporalmente, en breve un moderador revisará la"
                " notificación y actualizará el estado, lamentamos"
                " las molestias.")
        msg = msg.format(
                  upload.uri, upload.uri,
                  url_for('media', media_type=upload.media.media_type,
                          title_unique=upload.media.title_unique),
                  upload.media.title,
                  url_for('dcma_report', encoded_uri=upload.encode(upload.uri)),
                  dcma.name,
                  dcma.email,
              )
        upload.uploader.notify(msg)

        flash('Ha reportado correctamente "{}", '
              'su solicitud será procesada a la brevedad'.format(dcma_id))

    return redirect(url_for('index'))

@app.route('/user/resend')
@app.route('/reenviar/confirmacion')
@login_required
def user_resend():
    send_confirmation_email(current_user)
    flash('Se ha enviado un nuevo mensaje de confirmación a {}, '
          'por favor verifique su cuenta para continuar'
          .format(current_user.email))
    return redirect(url_for('user_unconfirmed'))

@app.route('/user/recover',     methods=['GET', 'POST'])
@app.route('/restaurar/cuenta', methods=['GET', 'POST'])
def user_recover():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = PasswordRecoverForm()
    if request.method == 'POST' and form.validate():
        user = User.objects(email=form.email.data).first()
        if user is None:
            flash('Se ha enviado un código de recuperación a {}, '
                  'por favor verifique su cuenta para continuar'
                  .format(form.email.data))
            return redirect(url_for('login'))
        else:
            if user.is_confirmed:
                send_reset_passwd_email(user)
                flash('Se ha enviado un código de recuperación a {}, '
                      'por favor verifique su cuenta para continuar'
                      .format(form.email.data))
                return redirect(url_for('login'))
            else:
                flash('El usuario con la cuenta {} no ha válidado su dirección, '
                      'inicie sesión y solicite su código de verificación'
                      .format(form.email.data))
                return redirect(url_for('login'))
    return render_template('user_recover.html.j2', title='Recuperar cuenta', form=form)

@app.route('/user/reset/<token>')
@app.route('/recuperar/cuenta/<token>')
def user_reset(token):
    email = confirm_email_token(token)
    if not email:
        flash('El código de recuperación es inválido o ha expirado')
        return redirect(url_for('user_recover'))

    user = User.objects(email=email).first()
    login_user(user)
    return redirect(url_for('user_reset_password'))

@app.route('/user/reset',       methods=['GET', 'POST'])
@app.route('/recuperar/cuenta', methods=['GET', 'POST'])
@login_required
@user_confirmed_required
def user_reset_password():
    form = PasswordResetForm()
    if request.method == 'POST' and form.validate():
        user = User.objects(id=current_user.id).first()
        user.reset_password(form.password.data).save()
        flash('Ha restaurado correctamente su cuenta')
        return redirect(url_for('index'))
    return render_template('user_reset_password.html.j2', title='Restaurar contraseña', form=form)

@app.route('/user/edit',     methods=['GET', 'POST'])
@app.route('/editar/cuenta', methods=['GET', 'POST'])
@login_required
@user_confirmed_required
def user_edit():
    form = EditUserForm()
    if request.method == 'POST' and form.validate():
        user          = User.objects(id=current_user.id).first()
        form_username = User.objects(username=form.username.data).first()
        form_email    = User.objects(email=form.email.data).first()

        if form_username != None and user.id != form_username.id:
            flash('Ya existe un usuario con el mismo nombre, seleccione otro')
            return redirect(url_for('user_edit'))

        if form_email != None and user.id != form_email.id:
            flash('Ya existe un usuario con el mismo correo, seleccione otro')
            return redirect(url_for('user_edit'))

        user.username = form.username.data
        user.email    = form.email.data
        user.about_me = form.about_me.data

        if form.password.data != None:
            user.reset_password(form.password.data)

        user.save()
        flash('Los cambios han sido guardados.')
        return redirect(url_for('user', username=current_user.username))
    else:
        user               = User.objects(email=current_user.email).first()
        form.username.data = user.username
        form.email.data    = user.email
        form.about_me.data = user.about_me

    return render_template('user_edit.html.j2', form=form)

@app.route('/user/delete',   methods=['GET', 'POST'])
@app.route('/borrar/cuenta', methods=['GET', 'POST'])
@login_required
def user_delete():
    for media in Media.objects(uploader=user):
        media.uploader = None
        media.save()
    user.delete()
    logout_user()
    return redirect(url_for('index'))

@app.route('/acceso-restringido/moderadores')
@login_required
def user_not_mod():
    return render_template('user_not_mod.html.j2')

@app.route('/acceso-restringido/administradores')
@login_required
def user_not_admin():
    return render_template('user_not_admin.html.j2')

@app.route('/panel')
@app.route('/panel/')
@login_required
@user_confirmed_required
def panel():
    return redirect(url_for('panel_content'))

@app.route('/panel/contenido/')
@app.route('/panel/contenido/<int:page>')
@app.route('/panel/contenido/pag/<int:page>')
@app.route('/panel/contenido/<media_type>')
@app.route('/panel/contenido/<media_type>/')
@app.route('/panel/contenido/<media_type>/<int:page>')
@app.route('/panel/contenido/<media_type>/pag/<int:page>')
@login_required
@user_confirmed_required
def panel_content(media_type="peli", page=1):
    user       = User.objects(username=current_user.username).first()
    media_type = validate_media_type(media_type)

    if user == None:
        flash('El usuario "%s" no existe.' % username)
        return redirect(url_for('index'))
    else:
        media   = Media.objects(media_type=media_type)
        uploads = Upload.objects(uploader=user).order_by('-id')
        uploads = uploads.filter(media__in=media)
        uploads = uploads.paginate(page=page, per_page=PER_PAGE)

    return render_template('panel_content.html.j2', user=user, uploads=uploads, media_type=media_type)

@app.route('/panel/moderador/')
@app.route('/panel/moderador/<media_type>')
@app.route('/panel/moderador/<media_type>/')
@login_required
@user_confirmed_required
@user_mod_required
def panel_mod(media_type="pelis"):
    user       = User.objects(username=current_user.username).first()
    media_type = validate_media_type(media_type)

    uploads = defaultdict(list)
    for upload in Upload.objects(Q(status="REPORTED") | Q(status="DCMA")).order_by('-id'):
        uploads[upload.media.media_type].append(upload)
        uploads[MEDIA_TYPE_MAPPING[upload.media.media_type]].append(upload)

    # app.logger.debug(uploads)
    return render_template('panel_mod.html.j2', user=user, uploads=uploads, media_type=media_type)

@app.route('/panel/administrador/')
@app.route('/panel/administrador/<status>')
@app.route('/panel/administrador/<status>/')
@login_required
@user_confirmed_required
@user_admin_required
def panel_admin(status="activos"):
    users, active_users, silenced_users, suspended_users = ([], [],[],[])

    for user in User.objects(username__ne=current_user.username).order_by('+registered_on'):
        if   user.status[-1]['state'] == "ACTIVE":
            active_users.append(user)
        elif user.status[-1]['state'] == "SILENCED":
            silenced_users.append(user)
        elif user.status[-1]['state'] == "SUSPENDED":
            suspended_users.append(user)
    
    if   status == "activos":
        users = active_users
    elif status == "reportados":
        users = silenced_users
    elif status == "suspendidos":
        users = suspended_users

    return render_template('panel_admin.html.j2',
                           users = users,
                           active_users_len=len(active_users),
                           silenced_users_len=len(silenced_users),
                           suspended_users_len=len(suspended_users),
                           status=status)

@app.route('/panel/favoritos/')
@app.route('/panel/favoritos/<media_type>')
@app.route('/panel/favoritos/<media_type>/')
@login_required
@user_confirmed_required
def panel_favoritos(media_type="peli"):
    user       = User.objects(username=current_user.username).first()
    media_type = validate_media_type(media_type)

    if user == None:
        flash('El usuario "%s" no existe.' % username)
        return redirect(url_for('index'))
    else:
        favorites = user.favorites()

    media = []

    for f in favorites:
        if not hasattr(f, 'media_type'):
            #rm empty references
            user.unfavorite(f).save()
            continue
        if f.media_type == media_type:
            media.append(f)

    return render_template('panel.html.j2', user=user, media=media, media_type=media_type)

@app.route('/perfil/')
@app.route('/perfil/<username>')
@app.route('/perfil/<username>/<int:page>')
@app.route('/perfil/<username>/pag/<int:page>')
@app.route('/perfil/<username>/<action>', methods=['GET','POST'])
@app.route('/perfil/<username>/<action>/<parameter>')
@app.route('/perfil/<username>/<action>/<parameter>/')
@app.route('/perfil/<username>/<action>/<parameter>/<int:page>')
@app.route('/perfil/<username>/<action>/<parameter>/pag/<int:page>')
@login_required
@user_confirmed_required
def profile(username=None, action="compartidos", parameter="peli", page=1):
    if username == None:
        user = User.objects(username=current_user.username).first()
    else:
        user = User.objects(username=username).first()

    if   user == None:
        flash('El usuario "{}" no existe.'.format(username))

        # app.logger.debug(request.referrer)
        if request.referrer is None:
            return redirect('index')
        elif "/" + username in request.referrer:
            return render_template('404.html.j2'), 404
        else:
            return redirect(request.referrer)

    elif username == None or username == current_user.username:
        return redirect(url_for('profile_favorites'))

    if action == "compartidos":
        media_type = validate_media_type(parameter)

        uploads = user.uploads(media_type, status="ACTIVE")
        uploads = uploads.paginate(page=page, per_page=PER_PAGE)
        return render_template('profile.html.j2', user=user, uploads=uploads, media_type=media_type)

    elif action == "silenciar" or action == "silenciar-":
        if user.is_mod():
            flash("No se puede moderar a un administrador/moderador, "
                  "permiso denegado")
        elif user.username == "fantasma":
            flash("No se puede silenciar la cuenta de respaldo 'fantasma'")
        else:
            if current_user.is_mod():
                if   action == "silenciar":
                    if user.is_silenced():
                        flash("El usuario ya ha sido silenciado")
                    else:
                        user.silence()
                        token = generate_status_token(user.username, user.status[-1]['state'])
                        token_url = url_for('confirm', item="estado", token=token, _external=True)
                        app.logger.debug("URL de token de estado: {}".format(token_url))
                        msg = ("Lo sentimos, <a href='{}'>{}</a> ha retirado"
                               " su permiso para reportar links rotos, esta"
                               " medida por lo general es temporal, por favor"
                               " tome unos minutos para revisar los lineamentos"
                               " del sitio y confirme"
                               " de <a href='{}'>recibido</a> una vez haya"
                               " concluido para que un moderador retome su"
                               " caso."
                               )
                        msg = msg.format(
                              url_for('profile', username=current_user.username),
                              current_user.username,
                              token_url,
                            )
                        user.notify(msg)
                        flash("Ha silenciado a {}".format(user.username))

                elif action == "silenciar-":
                    if user.is_silenced():
                        user.unsilence()
                        msg = ("¡En horabuena!, <a href='{}'>{}</a> ha retomado"
                               " su caso y resuelto restablecer su cuenta. Ahora"
                               " puede continuar reportando links rotos, gracias"
                               " por su colaboración. Juntos hacemos de"
                               " <a href='{}'>{}</a> el mejor sitio para compartir"
                               " contenido."
                               )
                        msg = msg.format(
                              url_for('profile', username=current_user.username),
                              current_user.username,
                              app.config['APP_URL'], app.config['APP_URL'],
                            )
                        user.notify(msg)
                        flash("Ha dejado de silenciar a {}".format(user.username))
            else:
                flash("Sólo un moderador/administrador puede "
                      "silenciar a un usuario, permiso denegado")

    elif action == "suspender" or action == "suspender-":
        if user.is_mod():
            flash("No se puede moderar a un administrador/moderador, "
                  "permiso denegado")
        elif user.username == "fantasma":
            flash("No se puede suspender la cuenta de respaldo 'fantasma'")
        else:
            if current_user.is_mod():
                if   action == "suspender":
                    if user.is_suspended():
                        flash("El usuario ya ha sido suspendido")
                    else:
                        user.suspend()
                        token = generate_status_token(user.username, user.status[-1]['state'])
                        token_url = url_for('confirm', item="estado", token=token, _external=True)
                        msg = ("Lo sentimos, <a href='{}'>{}</a> ha suspendido"
                               " su cuenta, esta medida por lo general es"
                               " temporal aunque tampoco se hace a la ligera,"
                               " por favor tome algunos minutos para revisar"
                               " los lineamientos del sitio y confirme de"
                               " <a href='{}'>recibido</a> una vez haya"
                               " concluido para que un moderador retome su"
                               " caso."
                               )
                        msg = msg.format(
                              url_for('profile', username=current_user.username, _external=True),
                              current_user.username,
                              token_url,
                            )
                        user.notify(msg, email=True)
                        flash("Ha suspendido a {}".format(user.username))
                        app.logger.debug("URL de token de estado: {}".format(token_url))

                elif action == "suspender-":
                    if user.is_suspended():
                        user.unsuspend()
                        msg = ("¡En horabuena!, <a href='{}'>{}</a> ha retomado"
                               " su caso y resuelto restablecer su cuenta."
                               " Gracias por su colaboración."
                               )
                        msg = msg.format(
                              url_for('profile', username=current_user.username, _external=True),
                              current_user.username,
                            )
                        user.notify(msg, email=True)
                        flash("Ha retirado la suspensión a {}".format(user.username))
            else:
                flash("Sólo un moderador/administrador puede "
                      "suspender a un usuario, permiso denegado")

    elif action == "eliminar":
        if user.is_mod():
            flash("No se puede eliminar a un administrador/moderador, "
                  "permiso denegado")
        elif user.username == "fantasma":
            flash("No se puede eliminar la cuenta de respaldo 'fantasma'")
        else:
            ghost_user = User.objects(username="fantasma").first()

            if ghost_user is None:
                ghost_user = User(
                            username="fantasma",
                            email="fantasma@" + app.config['APP_DOMAIN'],
                            password='0',
                            about_me='¡Boo!, ¡soy un fantasma!, \o.o/ doy miedo ...',
                            phrase='Estoy aquí para tomar el lugar de las cuentas eliminadas',
                            confirmed=True,
                            confirmed_on=datetime.now(),
                            ).save()

            Upload.objects(uploader=user).update(uploader=ghost_user)
            user.delete()
            flash("Se ha eliminado la cuenta de '{}'".format(username))
    
    elif action == "editar":
        form = EditUserForm()
        
        if current_user.is_admin() or current_user.is_mod():
            
            if request.method == 'POST' and form.validate():
                user          = User.objects(username=username).first()
                form_username = User.objects(username=form.username.data).first()
                form_email    = User.objects(email=form.email.data).first()
                
                if form_username != None and user.id != form_username.id:
                    flash('Ya existe un usuario con el mismo nombre, seleccione otro')
                    return redirect('/perfil/{}/editar'.format(username))
                
                if form_email != None and user.id != form_email.id:
                    flash('Ya existe un usuario con el mismo correo, seleccione otro')
                    return redirect('/perfil/{}/editar'.format(username))

                user.username = form.username.data
                user.email    = form.email.data
                user.about_me = form.about_me.data
                user.phrase   = form.phrase.data

                if form.password.data:
                    user.reset_password(form.password.data)
                    info = '¡Su contraseña se ha actualizado'

                text = ('<h4>{} ha Actualizado sus Datos:</h4>'.format(current_user.username)+
                        '<p>Nombre de Usuario: {}</p>'.format(user.username)+
                        '<p>Correo: {}</p>'.format(user.email)+
                        '<p>Sobre mi: {}</p>'.format(user.about_me)+
                        '<p>Frase: {}</p>'.format(user.phrase)+
                        '<h5>{}</h5>'.format(info))

                msg = text.format(url_for('profile', username=user.username), user.username)
                send_email(APP_FROM, form.email.data, text, msg)
                user.notify(msg)
                user.save()

                flash('Los cambios han sido guardados.')
                return redirect(url_for('panel_admin'))
            
            else:
                user               = User.objects(username=username).first()
                form.username.data = user.username
                form.email.data    = user.email
                form.about_me.data = user.about_me
                form.phrase.data   = user.phrase
                form.password.data = user.password
            
            return render_template("user_edit.html.j2.orig", form=form, user=user)
        
        else:
            flash('Acción no Permitida, no tiene autorización para modificar datos de los usuarios')
            return redirect(url_for('index'))

    elif action == "conmutar":
        profile = parameter

        if user.username == "fantasma":
            flash("No se puede modificar la cuenta de respaldo 'fantasma', "
                  "permiso denegado")

        elif profile == "moderador":
            if user.is_admin():
                flash("El usuario es administrador, permiso denegado")
            else:
                if user.is_mod():
                    if current_user.is_admin():
                        user.profile = "user"
                        msg = ("Lo sentimos, <a href='{}'>{}</a> ha retirado"
                               " sus permisos de moderación")
                        msg = msg.format(
                              url_for('profile', username=current_user.username),
                              current_user.username,
                            )
                        user.notify(msg)
                        flash("Se le han retirado los permisos de moderación "
                              "a {}".format(user.username))
                    else:
                        flash("Sólo un administrador puede remover permisos "
                              "de moderación, permiso denegado")
                else:
                    if current_user.is_mod():
                        user.profile = "mod"
                        msg = ("¡En horabuena!, <a href='{}'>{}</a> le ha"
                               " otorgado permisos de moderación, gracias"
                               " por ser parte de <a href='{}'>{}</a>")
                        msg = msg.format(
                              url_for('profile', username=current_user.username),
                              current_user.username,
                              app.config['APP_URL'], app.config['APP_URL'],
                            )
                        user.notify(msg)
                        flash("¡En horabuena!, se le han dado permisos de "
                              "moderación a {}".format(user.username))
                    else:
                        flash("Sólo un moderador o administrador puede otorgar "
                              "permisos de moderación, permiso denegado")
        elif profile == "admin" or profile == "administrador":
            if user.is_admin():
                flash("El usuario es administrador, permiso denegado")
            else:
                if current_user.is_admin():
                    user.profile = "admin"
                    msg = ("¡En horabuena!, <a href='{}'>{}</a> le ha"
                           " otorgado permisos de administración, gracias"
                           " por ser parte de <a href='{}'>{}</a>")
                    msg = msg.format(
                          url_for('profile', username=current_user.username),
                          current_user.username,
                          app.config['APP_URL'], app.config['APP_URL'],
                        )
                    user.notify(msg)
                    flash("¡En horabuena!, se le han dado permisos de "
                          "administración a {}".format(user.username))
                else:
                    flash("Sólo un administrador puede otorgar permisos "
                          "de administración, permiso denegado")
        user.save()

    return redirect(url_for('profile', username=user.username))

@app.route('/perfil/favoritos/')
@app.route('/perfil/favoritos/<media_type>')
@app.route('/perfil/favoritos/<media_type>/')
@login_required
@user_confirmed_required
def profile_favorites(media_type="peli"):
    user       = User.objects(username=current_user.username).first()
    media_type = validate_media_type(media_type)

    if user == None:
        flash('El usuario "%s" no existe.' % username)
        return redirect(url_for('index'))
    else:
        favorites = user.favorites()

    media = []

    for f in favorites:
        if not hasattr(f, 'media_type'):
            #rm empty references
            user.unfavorite(f).save()
            continue
        if f.media_type == media_type:
            media.append(f)

    return render_template('profile_favorites.html.j2',
                           user=user, media=media, media_type=media_type)

@app.route('/perfil/editar',  methods=['GET', 'POST'])
@app.route('/perfil/editar/', methods=['GET', 'POST'])
@login_required
@user_confirmed_required
def profile_edit():
    form = EditUserForm()
    if request.method == 'POST' and form.validate():
        user          = User.objects(id=current_user.id).first()
        form_username = User.objects(username=form.username.data).first()
        form_email    = User.objects(email=form.email.data).first()

        if form_username != None and user.id != form_username.id:
            flash('Ya existe un usuario con el mismo nombre, seleccione otro')
            return redirect(url_for('user_edit'))

        if form_email != None and user.id != form_email.id:
            flash('Ya existe un usuario con el mismo correo, seleccione otro')
            return redirect(url_for('user_edit'))

        user.username = form.username.data
        user.email    = form.email.data
        user.about_me = form.about_me.data
        user.phrase   = form.phrase.data

        if form.password.data: #is not empty
            user.reset_password(form.password.data)

        user.save()
        flash('Los cambios han sido guardados.')
        return redirect(url_for('profile'))
    else:
        user               = User.objects(id=current_user.id).first()
        form.username.data = user.username
        form.email.data    = user.email
        form.about_me.data = user.about_me
        form.phrase.data   = user.phrase

    return render_template('profile_edit.html.j2', user=user, form=form)

@app.route("/agregar/<media_type>", methods=['GET', 'POST'])
@login_required
@user_confirmed_required
def add_media(media_type):
    if   media_type == "peli":
        form = MovieForm()
    elif media_type == "serie":
        form = SerieForm()
    elif media_type == "anime":
        form = AnimeForm()
    elif media_type == "documental":
        form = DocumentalForm()
    else:
        flash('El medio no es "peli|serie|anime|documental", '
              'error: tipo "{}" desconocido'.format(media_type))
        return redirect(request.referrer)

    user = User.objects(id=current_user.id).first()
    if request.method == 'POST' and form.validate():
        moviedb_uri = sanitazer_uri(form.moviedb_uri.data)
        uri         = form.uri.data
        quality     = form.quality.data
        audio       = form.audio.data
        subtitle    = form.subtitle.data

        if media_type != "peli": #serie|anime|documental
            season  = form.season.data
            chapter = form.chapter.data

        if not validate_link(uri) or parse_tmdb_id(moviedb_uri) is None:

            if parse_tmdb_id(moviedb_uri) is None:
                flash('La url especificada no es válida: {}, por favor '
                      'vuelva a intentar'.format(form.moviedb_uri.data))
            else:
                flash('La url {} no es válida para el proveedor "{}", '
                      'por favor vuelva a intentarlo'.format(uri, parse_provider(uri)))

            if   media_type == "peli":
                return render_template('movie_add.html.j2', title="Agregar Peli", form=form)
            elif media_type == "serie":
                return render_template('serie_add.html.j2', title="Agregar Serie", form=form)
            elif media_type == "anime":
                return render_template('anime_add.html.j2', title="Agregar Anime", form=form)
            elif media_type == "documental":
                return render_template('documental_add.html.j2', title="Agregar Documental", form=form)

        if   media_type == "peli":
            add_movie(moviedb_uri, uri, quality, audio, subtitle, user)
        elif media_type == "serie":
            add_serie(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
        elif media_type == "anime":
            add_anime(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
        elif media_type == "documental":
            add_documental(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)

        return render_template('media_add_processing.html.j2', title="Procesando Link", media_type=media_type)

    if   media_type == "peli":
        return render_template('movie_add.html.j2', title="Agregar Peli",  form=form)
    elif media_type == "serie":
        return render_template('serie_add.html.j2', title="Agregar Serie", form=form)
    elif media_type == "anime":
        return render_template('anime_add.html.j2', title="Agregar Anime", form=form)
    elif media_type == "documental":
        return render_template('documental_add.html.j2', title="Agregar Documental", form=form)

@app.route("/agregar/<media_type>/al-mayoreo", methods=['GET', 'POST'])
@login_required
@user_confirmed_required
@user_mod_required
def add_media_in_bulk(media_type):
    if validate_media_type(media_type):
        if media_type == "peli":
            form = MovieBulkForm()
        else:
            form = SerieBulkForm()
    else:
        flash('El medio no es "peli|serie|anime|documental", '
              'error: tipo "{}" desconocido'.format(media_type))
        return redirect(request.referrer)

    user = User.objects(id=current_user.id).first()
    if request.method == 'POST' and form.validate():
        bulk = form.bulk.data
        # quick one-liner to remove empty lines
        # https://stackoverflow.com/a/24172715/890858
        bulk = "\n".join([s for s in bulk.strip().splitlines() if s.strip()])
        form.bulk.data = bulk

        if validate_bulk_links(media_type, bulk.splitlines()):
            for entry in bulk.splitlines():

                if   media_type == "peli":
                    moviedb_uri, uri, quality, audio, subtitle = entry.split()
                    moviedb_uri = sanitazer_uri(moviedb_uri)
                    add_movie(moviedb_uri, uri, quality, audio, subtitle, user)
                elif media_type == "serie":
                    moviedb_uri, season, chapter, uri, quality, audio, subtitle = entry.split()
                    moviedb_uri = sanitazer_uri(moviedb_uri)
                    add_serie(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
                elif media_type == "anime":
                    moviedb_uri, season, chapter, uri, quality, audio, subtitle = entry.split()
                    moviedb_uri = sanitazer_uri(moviedb_uri)
                    add_anime(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)
                elif media_type == "documental":
                    moviedb_uri, season, chapter, uri, quality, audio, subtitle = entry.split()
                    moviedb_uri = sanitazer_uri(moviedb_uri)
                    add_documental(moviedb_uri, uri, quality, audio, subtitle, user, season, chapter)

                timeDelay = random.randrange(0, 3)
                time.sleep(timeDelay/10) #wait from 0 to 3 milliseconds

            return render_template('media_add_processing.html.j2',
                                   title="Procesando Links", media_type=media_type)

    if   media_type == "peli":
        return render_template('movie_add_in_bulk.html.j2', title="Agregar Pelis al mayoreo",  form=form)
    elif media_type == "serie":
        return render_template('serie_add_in_bulk.html.j2', title="Agregar Series al mayoreo", form=form)
    elif media_type == "anime":
        return render_template('anime_add_in_bulk.html.j2', title="Agregar Animes al mayoreo", form=form)
    elif media_type == "documental":
        return render_template('documental_add_in_bulk.html.j2', title="Agregar Documentales al mayoreo", form=form)

@app.route("/pelis/")
@app.route("/pelis/<int:page>")
@app.route("/pelis/pag/<int:page>")
@app.route("/pelis/<filter>")
@app.route("/pelis/<filter>/")
@app.route("/pelis/<filter>/<int:page>")
@app.route("/pelis/<filter>/pag/<int:page>")
def pelis(filter="ultimas", page=1):
    if filter == "ultimas":
        #oldest to newest
        latest_pelis = Media.objects(media_type="peli") \
                       .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
        latest_pelis.items = media_with_published_uploads(latest_pelis.items)
        return render_template("index.html.j2", subtitle="Últimas pelis", media=latest_pelis)

    elif filter == "en-cartelera":
        #oldest to newest
        latest_pelis = Media.objects(media_type="peli") \
                       .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
        latest_pelis.items = media_with_published_uploads(latest_pelis.items)

        #since TMDB doesn't provide a easy way to check for is_playing status
        #per movie, use 60 days as a raw daycount approximation
        datetime_cartelera = datetime.now() - timedelta(days=60)

        cartelera_pelis = []
        for peli in latest_pelis.items:
            if peli.release_date > datetime_cartelera:
                cartelera_pelis.append(peli)

        latest_pelis.items = cartelera_pelis

        return render_template("index.html.j2", subtitle="Pelis en cartelera", media=latest_pelis)

    elif filter == "populares":
        popular_pelis = Media.objects(media_type="peli") \
                       .order_by('-score').paginate(page=page, per_page=PER_PAGE)
        popular_pelis.items = media_with_published_uploads(popular_pelis.items)
        return render_template("index.html.j2", subtitle="Las pelís más populares", media=popular_pelis)

    else:
        return render_template('404.html.j2'), 404

@app.route("/series/")
@app.route("/series/<int:page>")
@app.route("/series/pag/<int:page>")
@app.route("/series/<filter>")
@app.route("/series/<filter>/")
@app.route("/series/<filter>/<int:page>")
@app.route("/series/<filter>/pag/<int:page>")
def series(filter="ultimas", page=1):
    if filter == "ultimas":
        #oldest to newest
        latest_series = Media.objects(media_type="serie") \
                       .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
        latest_series.items = media_with_published_uploads(latest_series.items)
        return render_template("index.html.j2", subtitle="Últimas Series", media=latest_series)

    elif filter == "populares":
        popular_series = Media.objects(media_type="serie") \
                       .order_by('-score').paginate(page=page, per_page=PER_PAGE)
        popular_series.items = media_with_published_uploads(popular_series.items)
        return render_template("index.html.j2", subtitle="Las series más populares", media=popular_series)

    else:
        return render_template('404.html.j2'), 404

@app.route("/animes/")
@app.route("/animes/<int:page>")
@app.route("/animes/pag/<int:page>")
@app.route("/animes/<filter>")
@app.route("/animes/<filter>/")
@app.route("/animes/<filter>/<int:page>")
@app.route("/animes/<filter>/pag/<int:page>")
def animes(filter="ultimos", page=1):
    if filter == "ultimos" or filter == "ultimas":
        #oldest to newest
        latest_animes = Media.objects(media_type="anime")  \
                       .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
        latest_animes.items = media_with_published_uploads(latest_animes.items)
        return render_template("index.html.j2", subtitle="Últimos Animes", media=latest_animes)

    elif filter == "populares":
        popular_animes = Media.objects(media_type="anime") \
                       .order_by('-score').paginate(page=page, per_page=PER_PAGE)
        popular_animes.items = media_with_published_uploads(popular_animes.items)
        return render_template("index.html.j2", subtitle="Los animes más populares", media=popular_animes)

    else:
        return render_template('404.html.j2'), 404

@app.route("/documentales/")
@app.route("/documentales/<int:page>")
@app.route("/documentales/pag/<int:page>")
@app.route("/documentales/<filter>")
@app.route("/documentales/<filter>/")
@app.route("/documentales/<filter>/<int:page>")
@app.route("/documentales/<filter>/pag/<int:page>")
def documentales(filter="ultimos", page=1):
    if filter == "ultimos" or filter == "ultimas":
        #oldest to newest
        latest_documentales = Media.objects(media_type="documental")  \
                              .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
        latest_documentales.items = media_with_published_uploads(latest_documentales.items)
        return render_template("index.html.j2", subtitle="Últimos Documentales", media=latest_documentales)

    elif filter == "populares":
        popular_documentales = Media.objects(media_type="documental") \
                              .order_by('-score').paginate(page=page, per_page=PER_PAGE)
        popular_documentales.items = media_with_published_uploads(popular_documentales.items)
        return render_template("index.html.j2", subtitle="Los documentales más populares", media=popular_documentales)

    else:
        return render_template('404.html.j2'), 404

@app.route("/categorias")
@app.route("/categorias/")
def categories():
    return render_template('categories.html.j2',  title="Categorías",
                           subtitle="Categorías", genres=list(GENRE_MAPPING.keys()))

@app.route("/categoria/<category>", methods=['GET', 'POST'])
@app.route("/categoria/<category>/<int:page>", methods=['GET', 'POST'])
@app.route("/categoria/<category>/pag/<int:page>", methods=['GET', 'POST'])
def media_in_category(category, page=1):
    if category not in GENRE_MAPPING:
        return render_template('404.html.j2'), 404

    #oldest to newest
    latest = Media.objects(genres__in=GENRE_MAPPING[category]) \
             .order_by('-counter').paginate(page=page, per_page=PER_PAGE)
    latest.items = media_with_published_uploads(latest.items)

    return render_template('index.html.j2', title=category.capitalize(),
                           subtitle="Categoria {}:".format(category),
                           media=latest)

@app.route("/ruleta-rusa")
def media_random():
    media = Media.objects().first().random()
    return redirect(url_for('media', media_type=media.media_type, title_unique=media.title_unique))

@app.route("/<media_type>/<title_unique>")
def media(media_type, title_unique):
    media   = Media.objects(title_unique=title_unique).first()
    if media is None:
        return render_template('404.html.j2'), 404

    if   media_type == "peli":
        uploads = Upload.objects(media=media, status="ACTIVE")
        return render_template('media.html.j2', title=media.title, media=media, uploads=uploads)
    elif media_type == "serie" or media_type == "anime" or media_type == "documental":
        uploads          = Upload.objects(media=media, status="ACTIVE")
        chapters         = defaultdict(dict)
        ordered_chapters = defaultdict(dict)

        for upload in uploads:
            chapters[upload.episode.season][upload.episode.chapter] = upload.episode

        for season, chapter in sorted(chapters.items()):
            for chapter_number, episode in sorted(chapter.items()):
                ordered_chapters[season][chapter_number] = episode

        return render_template('chapters.html.j2',
                               title=media.title, media=media, chapters=ordered_chapters)
    else:
        return render_template('404.html.j2'), 404

@app.route("/<media_type>/<title_unique>/editar/metadatos",  methods=['GET', 'POST'])
@app.route("/<media_type>/<title_unique>/editar/metadatos/", methods=['GET', 'POST'])
@login_required
@user_confirmed_required
@user_admin_required
def edit_media_metadata(media_type, title_unique):
    media = Media.objects(title_unique=title_unique).first()

    if media is None:
        return render_template('404.html.j2'), 404

    form = MetaDataForm()

    if request.method == 'POST' and form.validate():
        media.title           = form.title.data
        media.title_unique    = get_title_unique(form.title.data)
        media.description     = form.description.data

        media.poster_uri      = form.poster_uri.data
        try:
            poster_b64        = get_poster_b64(form.poster_uri.data)
            media.poster_b64  = poster_b64
        except:
            pass

        media.fposter_uri     = form.fposter_uri.data
        try:
            poster_b64        = get_poster_b64(form.fposter_uri.data)
            media.fposter_b64 = poster_b64
        except:
            pass

        media.bposter_uri     = form.bposter_uri.data
        try:
            poster_b64        = get_poster_b64(form.bposter_uri.data)
            media.bposter_b64 = poster_b64
        except:
            pass

        media.lposter_uri     = form.lposter_uri.data
        try:
            poster_b64        = get_poster_b64(form.lposter_uri.data)
            media.lposter_b64 = poster_b64
        except:
            pass

        media.score   = form.score.data
        media.counter = form.counter.data
        # avoid running Media.clean(), since it will attempt to autocalculate counter
        media.save(clean=False)

        flash('Se han actualizado los metadatos de "{}" exitosamente'.format(title_unique))
        return redirect(url_for('media', media_type=media.media_type, title_unique=media.title_unique))

    else:
        form.title.data       = media.title
        form.description.data = media.description
        form.poster_uri.data  = media.poster_uri
        form.fposter_uri.data = media.fposter_uri
        form.bposter_uri.data = media.bposter_uri
        form.lposter_uri.data = media.lposter_uri
        form.score.data       = media.score
        form.counter.data     = media.counter

        return render_template('media_metadata.html.j2', title="Editar Metadatos", form=form, media=media)

@app.route("/<media_type>/<title_unique>/temporada-<int:season>/capitulo-<int:chapter>")
def chapters(media_type, title_unique, season, chapter):
    media   = Media.objects(title_unique=title_unique).first()
    episode = Episode.objects(media=media, season=season, chapter=chapter).first()

    if media is None or episode is None:
        return render_template('404.html.j2'), 404

    uploads = Upload.objects(media=media, episode=episode, status="ACTIVE")

    if media_type == "serie" or media_type == "anime" or media_type == "documental":
        media.title        = episode.title
        media.description  = episode.description
        media.fposter_uri  = episode.fposter_uri
        media.fposter_b64  = episode.fposter_b64
        media.release_date = episode.release_date
        media.score        = episode.score

        # app.logger.debug(uploads)
        return render_template('media.html.j2', title=media.title, media=media, episode=episode, uploads=uploads)
    else:
        return render_template('404.html.j2'), 404

@app.route("/publicar/<media_type>/<title_unique>/<upload_hash>")
@login_required
@user_confirmed_required
def upload_publish(media_type, title_unique, upload_hash):
    upload = Upload.objects(hash=upload_hash).first()
    user   = User.objects(id=current_user.id).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if user == upload.uploader or user.is_mod():
        if media_type == "peli":
            title = upload.media.title
        else:
            title = upload.episode.title

        if upload.status == "DCMA":
            dcma = Dcma.objects(remote_uri=upload.uri, confirmed=True).first()
            dcma.delete()

        upload.status = "ACTIVE"
        if user.is_mod(): upload.strikes = []
        upload.save()

        flash('Se ha publicado "%s"' % title)

        if user.is_mod() and upload.uploader != user:
            msg = ("Su link <a href='{}'>{}</a> de <a href='{}'>{}</a>"
                   " ha sido verificado por un moderador y ya se"
                   " encuentra en línea, lamentamos las molestias.")
            msg = msg.format(
                  upload.uri, upload.uri,
                  url_for('media', media_type=upload.media.media_type,
                  title_unique=upload.media.title_unique), upload.media.title,
                )
            upload.uploader.notify(msg)
    else:
        flash('No está autorizado para publicar este elemento, '
              'sólo el admin/mod o el uploader pueden hacerlo')

    return redirect(request.referrer)

@app.route("/desactivar/<media_type>/<title_unique>/<upload_hash>")
@login_required
@user_confirmed_required
def upload_unpublish(media_type, title_unique, upload_hash):
    upload = Upload.objects(hash=upload_hash).first()
    user   = User.objects(id=current_user.id).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if user == upload.uploader or user.is_mod():
        if media_type == "peli":
            title = upload.media.title
        else:
            title = upload.episode.title

        upload.status = "PREVIEW"
        upload.save()

        flash('Se ha desactivado "%s"' % title)
    else:
        flash('No está autorizado para publicar este elemento, '
              'sólo el admin/mod o el uploader pueden hacerlo')

    return redirect(request.referrer)

@app.route("/reportar/<upload_hash>")
@login_required
@user_confirmed_required
def report(upload_hash):
    upload = Upload.objects(hash=upload_hash).first()
    user   = User.objects(id=current_user.id).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if upload.media.media_type == "peli":
        title = upload.media.title
    else:
        title = upload.episode.title

    upload.strikes.append(user)

    if not user.is_mod():
        msg = ("Hola!, <a href='{}'>{}</a> reportó un posible link roto"
               " en <a href='{}'>{}</a> de <a href='{}'>{}</a>, puede"
               " editarlo desde <a href='{}'>aquí</a>.")
        msg = msg.format(
                url_for('profile', username=user.username), user.username,
                upload.uri, upload.uri,
                url_for('media', media_type=upload.media.media_type,
                title_unique=upload.media.title_unique), upload.media.title,
                url_for('upload_edit', media_type=upload.media.media_type,
                title_unique=upload.media.title_unique, upload_hash=upload.hash),
              )
        upload.uploader.notify(msg)

    if len(upload.strikes) >= app.config['APP_MAX_STRIKES'] or user.is_mod():
        upload.status = "REPORTED"

        if len(upload.strikes) >= app.config['APP_MAX_STRIKES']:
            msg = ("Ops!, el link <a href='{}'>{}</a> de <a href='{}'>{}</a>"
                   " alcanzó el número máximo de strikes permitidos ({}), será"
                   " deshabilitado temporalmente mientras los moderadores lo"
                   " verifican, lamentamos las molestias.")
            msg = msg.format(
                  url_for('media', media_type=upload.media.media_type,
                  title_unique=upload.media.title_unique), upload.media.title,
                  app.config['APP_MAX_STRIKES'],
                )
        elif user.is_mod():
            msg = ("Hola!, <a href='{}'>{}</a> <strong>(moderador)</strong>"
                   " reportó un posible link roto"
                   " en <a href='{}'>{}</a> de <a href='{}'>{}</a>, puede"
                   " editarlo desde <a href='{}'>aquí</a> o eliminarlo desde el"
                   " <a href='{}'>panel de control</a>. Por lo pronto, se"
                   " deshabilitará para evitar confusiones, lamentamos las"
                   " molestias.")
            msg = msg.format(
                    url_for('profile', username=user.username), user.username,
                    upload.uri, upload.uri,
                    url_for('media', media_type=upload.media.media_type,
                    title_unique=upload.media.title_unique), upload.media.title,
                    url_for('upload_edit', media_type=upload.media.media_type,
                    title_unique=upload.media.title_unique, upload_hash=upload.hash),
                    url_for('panel_content'),
                  )
        upload.uploader.notify(msg)

    upload.save()

    flash('Se ha reportado el enlace de "{}", !gracias por ser parte '
          'activa de la comunidad!'.format(upload.uploader.username))

    return redirect(request.referrer)

@app.route("/deshacer-reporte/<upload_hash>")
@login_required
@user_confirmed_required
def unreport(upload_hash):
    upload = Upload.objects(hash=upload_hash).first()
    user   = User.objects(id=current_user.id).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if upload.media.media_type == "peli":
        title = upload.media.title
    else:
        title = upload.episode.title

    if upload.strikes.count(user) > 0:
        upload.strikes.remove(user)
        upload.save()

    flash('Se ha revertido el reporte contra "{}", '
          'sin resentimientos ;)'.format(upload.uploader.username))

    return redirect(request.referrer)

@app.route("/editar/<media_type>/<title_unique>/<upload_hash>", methods=['GET', 'POST'])
@login_required
@user_confirmed_required
def upload_edit(media_type, title_unique, upload_hash):
    upload = Upload.objects(hash=upload_hash).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if  upload.media.media_type  == "peli":
        form = MovieForm()

        if request.method == 'POST' and form.validate():
            user         = User.objects(id=current_user.id).first()
            moviedb_uri  = form.moviedb_uri.data
            uri          = form.uri.data
            download_uri = form.download_uri.data
            quality      = form.quality.data
            audio        = form.audio.data
            subtitle     = form.subtitle.data

            edit_movie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user)
            return render_template('media_add_processing.html.j2', title="Procesando Peli", media_type=media_type)
        else:
            form.moviedb_uri.data  = upload.media.moviedb_uri
            form.uri.data          = upload.uri
            form.download_uri.data = upload.download_uri
            form.quality.data      = upload.quality
            form.audio.data        = upload.audio
            form.subtitle.data     = upload.subtitle

            return render_template('movie_add.html.j2', title="Editar Peli", form=form)

    elif upload.media.media_type == "serie" or \
         upload.media.media_type == "anime" or \
         upload.media.media_type == "documental" :

        form = SerieForm()

        if request.method == 'POST' and form.validate():
            user         = User.objects(id=current_user.id).first()
            moviedb_uri  = form.moviedb_uri.data
            season       = form.season.data
            chapter      = form.chapter.data
            uri          = form.uri.data
            download_uri = form.download_uri.data
            quality      = form.quality.data
            audio        = form.audio.data
            subtitle     = form.subtitle.data

            edit_serie(upload_hash, moviedb_uri, uri, download_uri, quality, audio, subtitle, user, season, chapter)
            return render_template('media_add_processing.html.j2', title="Procesando Serie", media_type=media_type)
        else:
            form.moviedb_uri.data  = upload.media.moviedb_uri
            form.season.data       = upload.episode.season
            form.chapter.data      = upload.episode.chapter
            form.uri.data          = upload.uri
            form.download_uri.data = upload.download_uri
            form.quality.data      = upload.quality
            form.audio.data        = upload.audio
            form.audio.subtitle    = upload.subtitle

            if upload.media.media_type   == "serie":
                return render_template('serie_add.html.j2', title="Editar Serie", form=form)
            elif upload.media.media_type == "anime":
                return render_template('anime_add.html.j2', title="Editar Anime", form=form)
            elif upload.media.media_type == "documental":
                return render_template('documental_add.html.j2', title="Editar Documental", form=form)
    else:
        return render_template('404.html.j2'), 404

@app.route("/eliminar/<media_type>/<title_unique>/<season>/<chapter>")
@login_required
@user_confirmed_required
def episode_delete(media_type, title_unique, season, chapter):
    user    = User.objects(id=current_user.id).first()
    media   = Media.objects(media_type=media_type, title_unique=title_unique).first()
    episode = Episode.objects(media=media, season=season, chapter=chapter).first()

    if episode is None:
        return render_template('404.html.j2'), 404

    if user.is_admin():
        title = episode.title

        for upload in Upload.objects(media=media, episode=episode):
            cdn_delete(upload.uri); upload.delete()

        delete_media_leftovers(media, episode)
        flash('Se ha eliminado "%s"' % title)
    else:
        flash('No está autorizado para eliminar este elemento, '
              'sólo el admin puede hacerlo')

    return redirect(request.referrer)

@app.route("/eliminar/<media_type>/<title_unique>/<upload_hash>")
@login_required
@user_confirmed_required
def upload_delete(media_type, title_unique, upload_hash):
    upload = Upload.objects(hash=upload_hash).first()
    user   = User.objects(id=current_user.id).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if user == upload.uploader or user.is_admin():
        if media_type == "peli":
            title = upload.media.title
        else:
            title = upload.episode.title

        media   = Media.objects(id=upload.media.id).first()

        cdn_delete(upload.uri); upload.delete()
        flash('Se ha eliminado "%s"' % title)

        if media_type == "peli":
            delete_media_leftovers(media)
        else:
            episode = Episode.objects(id=upload.episode.id).first()
            delete_media_leftovers(media, episode)
    else:
        flash('No está autorizado para eliminar este elemento, '
              'sólo el admin o el uploader pueden hacerlo')

    return redirect(request.referrer)

@app.route('/agregar/favorito/<title_unique>')
@login_required
@user_confirmed_required
def favorite_add(title_unique):
    user  = User.objects(id=current_user.id).first()
    media = Media.objects(title_unique=title_unique).first()

    if media is None:
        flash('La pelí "%s" no existe, pruebe con otra' % title_unique)
        return redirect(url_for('panel'))

    user.favorite(media).save()
    flash('Se ha agregado "%s" a favoritos' % media.title)

    return redirect(request.referrer)

@app.route('/eliminar/favorito/<title_unique>')
@login_required
@user_confirmed_required
def favorite_delete(title_unique):
    user  = User.objects(id=current_user.id).first()
    media = Media.objects(title_unique=title_unique).first()

    if media is None:
        flash('La pelí "{}" no existe, pruebe con otra'.format(title_unique))
        return redirect(url_for('panel'))

    user.unfavorite(media).save()

    flash('Se ha eliminado "%s" de favoritos' % media.title)

    return redirect(request.referrer)

@app.route("/notificaciones")
@app.route("/notificaciones/")
@app.route("/notificaciones/<action>")
@login_required
@user_confirmed_required
def notifications(action=None):
    if action == "eliminar":
        current_user.notifications(seen="all").delete()
        flash("Se han eliminado todas las notificaciones")
        return redirect(request.referrer)
    elif action == "marcar-como-leidas":
        for notification in current_user.notifications(seen="new"):
            notification.seen = True
            notification.save()
        flash("Se han marcado como leídas todas las notificaciones")
        return redirect(request.referrer)
    elif action == "todas":
        notifications = current_user.notifications(seen="all")
    else:
        notifications = current_user.notifications()
    return render_template('notifications.html.j2', notifications=notifications)

@app.route("/notificacion/<action>/<hash>")
@login_required
@user_confirmed_required
def notification(action, hash):
    notification = Notification.objects(hash=hash).first()
    if   action == "eliminar":
        notification.delete()
    elif action == "marcar-como-leido":
        notification.seen = True
        notification.save()
    return redirect(request.referrer)

@app.route("/process/<encoded_uri>")
@login_required
@user_confirmed_required
def process(encoded_uri):
    uri    = decode_uri(encoded_uri)
    app.logger.debug("Process uri: {}".format(uri))
    upload = Upload.objects(uri=uri).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    if url_has_valid_mp4(upload.download_uri):
        upload.download_uri_timestamp = datetime.now(); upload.save()
        return redirect(url_for('ver', encoded_uri=encode_uri(upload.download_uri)))
    else:
        app.logger.debug("Refresh cache: {}".format(upload.download_uri))
        refresh_download_link(upload)
        return render_template('media_processing.html.j2',
                               title="Procesando Link", upload=upload)

@app.route("/process/<encoded_uri>/force")
@login_required
@user_confirmed_required
def process_force(encoded_uri):
    download_uri = decode_uri(encoded_uri)
    app.logger.debug("Forced refresh cache: {}".format(download_uri))
    upload = Upload.objects(download_uri=download_uri).first()

    if upload is None:
        return render_template('404.html.j2'), 404

    refresh_download_link(upload)
    return render_template('media_processing.html.j2',
                           title="Procesando Link", upload=upload)

@app.route("/v/", methods=['GET', 'POST'])
def ver_direct():
    form = DirectViewForm()
    if request.method == 'POST' and form.validate():
        return redirect(url_for('ver', encoded_uri=encode_uri(form.uri.data)))
    return render_template('ver_form.html.j2', form=form)

@app.route("/v/<encoded_uri>")
def ver(encoded_uri):
    uri = decode_uri(encoded_uri)
    next_uri = None
    skip_intro_secs = 35

    upload = Upload.objects(download_uri=uri).first()

    if upload:
        if upload.media.media_type == "peli":
            next_uri = url_for('media',
                               media_type=upload.media.media_type,
                               title_unique=upload.media.title_unique,
                       )
        else:
            next_episode = upload.episode.next()
            if next_episode:
                next_uri = url_for('ver', encoded_uri=encode_uri(next_episode.download_uri()))

    if re.search("^magnet:", uri):
        # return render_template('ver.magnet.flowplayer.html.j2', uri=uri)
        return render_template('ver.magnet.glowplayer.html.j2', uri=uri, next_uri=next_uri, skip_intro_secs=skip_intro_secs)
        # return render_template('ver.magnet.html.j2', uri=uri) #video tag
    elif re.search("^http(s)?:", uri):
        # return render_template('ver.direct.flowplayer.html.j2', uri=uri)
        return render_template('ver.direct.glowplayer.html.j2', uri=uri, next_uri=next_uri, skip_intro_secs=skip_intro_secs)
        # return render_template('ver.direct.videojs.html.j2', uri=uri, next_uri=next_uri)
    else:
        app.logger.debug(uri)
        flash('"{}" no es una dirección válida'.format(uri))
        return redirect(request.referrer)

@app.route("/d/", methods=['GET', 'POST'])
def descargar_direct():
    form = DirectDownloadForm()
    if request.method == 'POST' and form.validate():
        return redirect(url_for('download', uri=form.uri.data))
    return render_template('download_form.html.j2', form=form)

@app.route("/d/<encoded_uri>")
def download(encoded_uri):
    uri = decode_uri(encoded_uri)

    if uri is not None:
        if re.search("^magnet:", uri):
            return render_template('download.magnet.html.j2', uri=uri)
        elif re.search("^http(s)?:", uri):
            return redirect(uri)
        else:
            app.logger.debug(uri)
            flash('"{}" no es una dirección válida'.format(uri))
            return redirect(request.referrer)
    else:
        app.logger.debug(encoded_uri)
        flash('"{}" no es una dirección válida'.format(encoded_uri))
        return redirect(request.referrer)

@app.route("/dcma",  methods=['GET', 'POST'])
@app.route("/dcma/", methods=['GET', 'POST'])
def dcma():
    form = DcmaForm()
    if request.method == 'POST' and form.validate():
        remote_uri=form.remote_uri.data
        dcma = Dcma.objects(remote_uri=remote_uri, confirmed=True).first()

        if dcma:
            flash('La url "{}" ya ha sido reportada'.format(remote_uri))
        else:
            dcma = Dcma(
                     kind=form.kind.data,
                     name=form.name.data,
                     email=form.email.data,
                     phone_number=form.phone_number.data,
                     organization=form.organization.data,
                     position=form.position.data,
                     address=form.address.data,
                     zipcode=form.zipcode.data,
                     rightholder_name=form.rightholder_name.data,
                     rightholder_country=form.rightholder_country.data,
                     local_uri=form.local_uri.data,
                     remote_uri=form.remote_uri.data,
                   ).save()
            send_dcma_confirmation_email(dcma)
            return render_template('dcma_unconfirmed.html.j2')
    return render_template('dcma.html.j2', title='Reportar contenido', form=form)

@app.route("/dcma/<encoded_uri>")
@login_required
def dcma_report(encoded_uri):
    uri  = decode_uri(encoded_uri)
    dcma = Dcma.objects(remote_uri=uri, confirmed=True).first()

    if dcma is None:
        return render_template('404.html.j2'), 404

    return render_template('dcma_report.html.j2', dcma=dcma)

@app.route("/legal/<machote>")
def legal(machote):
    if   machote == "terminos-de-uso":
        return render_template('terminos-de-uso.html.j2')
    elif machote == "politica-de-privacidad":
        return render_template('politica-de-privacidad.html.j2')
    else:
        return render_template('404.html.j2'), 404

# TEST VIDEO ROUTE
@app.route("/mplayer")
@app.route("/mplayer/<type>")
def mplayer(type="default"):
    if type == "glow":
        return render_template('mplayer.glow.html.j2')
    else:
        return render_template('mplayer.html.j2')
# END TEST VIDEO ROUTE

# @app.route("/migrate/FULL_HD")
# def migrate_FULL_HD():
    # for upload in Upload.objects():
        # if upload.quality == "FULL HD":
            # upload.quality = "FULL_HD"
        # upload.save()

    # flash("migration sucessfully")
    # return redirect(url_for('index'))

@app.route("/migrate/media/counter")
@login_required
@user_confirmed_required
@user_admin_required
def migrate_media_counter():
    i = 0.0
    for media in Media.objects().order_by('+counter'):
        # app.logger.debug(media.title_unique + " => " + str(media.id) + " => " + str(i))
        media.counter = i
        i            += 1.0
        # avoid running Media.clean(), since it will attempt to autocalculate counter
        media.save(clean=False)

    flash("migration sucessfully")
    return redirect(url_for('index'))

@app.route('/exception')
def error():
    raise Exception("I'm sorry, Dave. I'm afraid I can't do that.")

@app.errorhandler(401)
def not_found_error(error):
    return render_template('401.html.j2'), 401

@app.errorhandler(404)
def not_found_error(error):
    app.logger.debug(error)
    return render_template('404.html.j2'), 404

@app.errorhandler(405)
def not_found_error(error):
    return render_template('405.html.j2'), 405

@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html.j2'), 500
