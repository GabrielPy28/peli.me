from config    import Config

from app       import app
from app.utils import encode_uri
from app.decorators import threaded

from random    import shuffle

import requests

def cdn_cache(uri):
    encoded_uri = encode_uri(uri)
    peers = cdn_peers()

    for peer in peers:
        peer_uri = "https://" + peer + "/" + encoded_uri

        try:
            peer_request = requests.head(
                               peer_uri,
                               auth=('key', Config.CDN_KEY),
                               verify=False,
                               timeout=3,
                               allow_redirects=True,
                           )

            if peer_request.ok:
                return "https://{}/{}".format(peer, encoded_uri)

        except requests.exceptions.Timeout:
            app.logger.debug("{} timeout!".format(peer_uri))

@threaded
def cdn_delete(uri):
    with app.app_context():
        encoded_uri = encode_uri(uri)
        peers = cdn_peers()

        for peer in peers:
            peer_uri = "https://" + peer + "/" + encoded_uri

            try:
                peer_request = requests.delete(
                                   peer_uri,
                                   auth=('key', Config.CDN_KEY),
                                   verify=False,
                                   timeout=3,
                                   allow_redirects=True,
                               )

                if peer_request.ok:
                    return "https://{}/{}".format(peer, encoded_uri)

            except requests.exceptions.Timeout:
                app.logger.debug("{} timeout!".format(peer_uri))

def cdn_peers():
    peers = Config.CDN_HOSTS.split(',')
    shuffle(peers) #in-place
    return peers
