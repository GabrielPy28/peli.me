About
-----

[peli.me](https://peli.me/)

Usage
-----

    #config
    $ echo 'APP_DOMAIN=peli.me'              >  .env
    $ echo 'APP_ADMIN=admin@peli.me'        >>  .env
    $ echo 'APP_FROM=no-reply@peli.me'      >>  .env

    $ echo 'SMTP_SERVER=smtp.peli.me'       >>  .env
    $ echo 'SMTP_PORT=25'                   >>  .env
    $ echo 'SMTP_USERNAME=no-reply@peli.me' >>  .env
    $ echo 'SMTP_PASSWORD=passwd'           >>  .env
    $ echo 'SMTP_USE_TLS=yes'               >>  .env

    $ echo 'MAILGUN_DOMAIN=domain.tld'      >> .env
    $ echo 'MAILGUN_API=key'                >> .env

    $ echo 'TMDB_API=key'                   >> .env
    $ echo 'CHATBRO_ID=id'                  >> .env
    $ echo 'CHATBRO_ANTISPOOFING_KEY=key'   >> .env
    $ echo 'RECAPTCHA_PUBLIC_KEY=key'       >> .env
    $ echo 'RECAPTCHA_PRIVATE_KEY=key'      >> .env

    $ echo 'CDN_HOSTS=cdn.peli.me,a.cdn.peli.me' >> .env
    $ echo 'CDN_KEY=default'                     >> .env

    #development
    $ ./setup.sh [docker-compose-file]

    #production
    $ docker run --rm -it -p 127.0.0.1:9150:9150 peterdavehello/tor-socks-proxy
    $ ./deploy.sh prod #in another window

    #production fast-forward (only updates app's code)
    $ docker run --rm -it -p 127.0.0.1:9150:9150 peterdavehello/tor-socks-proxy
    $ ./deploy-fast-forward.sh prod #in another window

Access http://localhost:5000

Dependencies
------------

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
