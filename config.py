import os
import urllib.parse

class Config(object):
    #general settings
    APP_DOMAIN = os.environ.get('APP_DOMAIN') or 'domain.tld'
    APP_ADMIN  = os.environ.get('APP_ADMIN')  or 'admin@'    + APP_DOMAIN
    APP_FROM   = os.environ.get('APP_FROM')   or 'no-reply@' + APP_DOMAIN
    APP_URL    = os.environ.get('APP_URL')    or 'https://'  + APP_DOMAIN

    APP_ITEMS_PER_PAGE = os.environ.get('APP_ITEMS_PER_PAGE') or 20
    APP_ITEMS_PER_PAGE = int(APP_ITEMS_PER_PAGE)

    APP_MAX_UPLOADS_BULK = os.environ.get('APP_MAX_UPLOADS_BULK') or 10
    APP_MAX_UPLOADS_BULK = int(APP_MAX_UPLOADS_BULK)

    APP_REGISTER = os.environ.get('APP_REGISTER') or  True
    APP_REGISTER = False if APP_REGISTER == "no" else True

    CDN_HOSTS = os.environ.get('CDN_HOSTS') or 'cdn.' + APP_DOMAIN
    CDN_KEY   = os.environ.get('CDN_KEY')   or 'you-will-never-guess'

    #flask-wtf
    SECRET_KEY  = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    #flask-mongoengine
    MONGODB_HOST     = os.environ.get('MONGODB_HOST')     or 'mongodb'
    MONGODB_TCP_PORT = os.environ.get('MONGODB_TCP_PORT') or 27017
    MONGODB_TCP_PORT = int(MONGODB_TCP_PORT)
    MONGODB_DB       = os.environ.get('MONGODB_DB')       or 'app'
    MONGODB_USER     = os.environ.get('MONGODB_USER')     or 'app'
    MONGODB_PASSWD   = os.environ.get('MONGODB_PASSWD')   or 'app'

    MONGODB_USER     = urllib.parse.quote_plus(MONGODB_USER)
    MONGODB_PASSWD   = urllib.parse.quote_plus(MONGODB_PASSWD)

    MONGODB_SETTINGS = {
        'db':       MONGODB_DB,
        'host':     MONGODB_HOST,
        'port':     MONGODB_TCP_PORT,
        'username': MONGODB_USER,
        'password': MONGODB_PASSWD,
    }

    APP_MAIL_PROVIDER = os.environ.get('APP_MAIL_PROVIDER') or 'SMTP'

    SMTP_SERVER    = os.environ.get('SMTP_SERVER') or 'localhost'
    SMTP_PORT      = os.environ.get('SMTP_PORT')   or 25
    SMTP_PORT      = int(SMTP_PORT)
    SMTP_USERNAME  = os.environ.get('SMTP_USERNAME') or 'guest'
    SMTP_PASSWORD  = os.environ.get('SMTP_PASSWORD') or 'you-will-never-guess'
    SMTP_USE_TLS   = os.environ.get('SMTP_USE_TLS')  or 'yes'

    MAILGUN_API    = os.environ.get('MAILGUN_API')    or 'you-will-never-guess'
    MAILGUN_DOMAIN = os.environ.get('MAILGUN_DOMAIN') or APP_DOMAIN

    #tmdb
    TMDB_API       = os.environ.get('TMDB_API') or 'you-will-never-guess'

    #recaptcha
    RECAPTCHA_PUBLIC_KEY  = os.environ.get('RECAPTCHA_PUBLIC_KEY')  or 'you-will-never-guess'
    RECAPTCHA_PRIVATE_KEY = os.environ.get('RECAPTCHA_PRIVATE_KEY') or 'you-will-never-guess'

    #chatbro
    CHATBRO_ID = os.environ.get('CHATBRO_ID') or 'you-will-never-guess'
    CHATBRO_ANTISPOOFING_KEY = os.environ.get('CHATBRO_ANTISPOOFING_KEY') or 'you-will-never-guess'

    #costly cpu/bandwith url verification
    APP_URL_PROVIDER_VERIFICATION = os.environ.get('APP_URL_PROVIDER_VERIFICATION') or True
    APP_MAX_STRIKES = os.environ.get('APP_MAX_STRIKES') or 3
    APP_FORWARD_REWARD_SECONDS = os.environ.get('APP_FORWARD_REWARD_SECONDS') or 10

    #enable/disable email sending
    APP_MAIL_SENDING = os.environ.get('APP_MAIL_SENDING') or  True
    APP_MAIL_SENDING = False if APP_MAIL_SENDING == "no" else True

    #per environment settings
    APP_ENVIRONMENT = os.environ.get('APP_ENVIRONMENT') or 'development'

    if   APP_ENVIRONMENT == 'development':
        DEBUG   = True
        TESTING = True #disable recaptcha
    elif APP_ENVIRONMENT == 'production' :
        DEBUG  = False
